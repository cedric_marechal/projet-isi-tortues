/*
 * http://fr.openclassrooms.com/informatique/cours/apprenez-a-programmer-en-java/mieux-structurer-son-code-le-pattern-mvc
 * http://www.infres.enst.fr/~hudry/coursJava/interSwing/boutons5.html
 */
package isitp4;

import Controleur.Controleur;
import Vue.FenetrePrincipale;

/**
 * Point d'entrée main de l'application
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class ISITP4 {

    /**
     * Crée le controleur et la fenêtre principale
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Logo demarre!");
        Controleur controleur = new Controleur();
        FenetrePrincipale fenetre = new FenetrePrincipale(controleur);
    }
}
