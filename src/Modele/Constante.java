package Modele;

import java.awt.Color;

/**
 * Les constantes mathématiques
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public final class Constante {

    public static final int rp = 10, rb = 5; // Taille de la pointe et de la base de la fleche
    public static final double ratioDegRad = 0.0174533; // Rapport radians/degres (pour la conversion)

    /**
     * Non instanciable
     */
    public Constante() {
        throw new AssertionError();
    }
    
     /**
     * Mapping entier -> couleur
     *
     * @param c l'entier codant une couleur
     * @return la couleur
     */
    public static Color decodeColor(int c) {
        switch (c) {
            case 0:
                return (Color.black);
            case 1:
                return (Color.blue);
            case 2:
                return (Color.cyan);
            case 3:
                return (Color.darkGray);
            case 4:
                return (Color.red);
            case 5:
                return (Color.green);
            case 6:
                return (Color.lightGray);
            case 7:
                return (Color.magenta);
            case 8:
                return (Color.orange);
            case 9:
                return (Color.gray);
            case 10:
                return (Color.pink);
            case 11:
                return (Color.yellow);
            default:
                return (Color.black);
        }
    }
}
