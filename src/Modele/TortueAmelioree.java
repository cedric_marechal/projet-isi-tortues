package Modele;

import Modele.Strategie.Cooperation.Collectif;
import Modele.Strategie.Position.Attaquant;
import Modele.Strategie.Strategie;
import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class TortueAmelioree extends Tortue {

    private String nom;
    private ArrayList<Tortue> listeTortuesConnues;
    private Strategie strategie;
    private int force, vision, equipe;
    private boolean demarque;

    /**
     *
     * @param x la position X en pixels
     * @param y la position Y en pixels
     * @param dir la direction en degrés de la tortue
     * @param vitesse la vitesse de la tortue en FPS
     * @param force la force de la tortue
     * @param vision le champ de vision de la tortue en pixels
     * @param equipe le numéro de l'équipe de la tortue
     * @param coul la couleur de la tortue
     * @param nom le nom de la tortue
     * @param strategie la stratégie de la tortue
     */
    public TortueAmelioree(int x, int y, int dir, int vitesse, int force, int vision, int equipe, Color coul, String nom, Strategie strategie) {
        super(x, y, dir, vitesse, coul);
        this.nom = nom;
        this.force = force;
        this.vision = vision;
        this.equipe = equipe;
        this.listeTortuesConnues = new ArrayList();
        this.strategie = strategie;
        this.demarque = true;
    }

    /**
     *
     */
    public TortueAmelioree() {
        super(100, 100, 90, 60, Color.BLUE);
        this.nom = "Inconnu";
        this.force = 100;
        this.vision = 20;
        this.equipe = 1;
        this.listeTortuesConnues = new ArrayList();
        this.strategie = new Strategie(new Attaquant(), new Collectif());
    }

    /**
     *
     * @return le nom de la tortue
     */
    public String getNom() {
        return nom;
    }

    /**
     * Met à jour le nom de la tortue puis notifie ses observeurs
     *
     * @param nom le nom de la tortue
     */
    public void setNom(String nom) {
        this.nom = nom;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la force de la tortue
     */
    public int getForce() {
        return force;
    }

    /**
     * Met à jour la force de la tortue puis notifie ses observeurs
     *
     * @param force la force de la tortue
     */
    public void setForce(int force) {
        this.force = force;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return le champ de vision de la tortue en pixels
     */
    public int getVision() {
        return vision;
    }

    /**
     * Met à jour le champ de vision de la tortue puis notifie ses observeurs
     *
     * @param vision le champ de vision de la tortue en pixels
     */
    public void setVision(int vision) {
        this.vision = vision;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return le numéro de l'équipe de la tortue
     */
    public int getEquipe() {
        return equipe;
    }

    /**
     * Met à jour le numéro de l'équipe de la tortue puis notifie ses observeurs
     *
     * @param equipe le numéro de l'équipe de la tortue
     */
    public void setEquipe(int equipe) {
        this.equipe = equipe;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la stratégie de la tortue
     */
    public Strategie getStrategie() {
        return strategie;
    }

    /**
     * Met à jour la stratégie de la tortue puis notifie ses observeurs
     *
     * @param strategie la stratégie de la tortue
     */
    public void setStrategie(Strategie strategie) {
        this.strategie = strategie;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la liste des tortues connues
     */
    public ArrayList<Tortue> getListeTortuesConnues() {
        return listeTortuesConnues;
    }

    /**
     *
     * @param listeTortuesConnues la liste des tortues connues
     */
    public void setListeTortuesConnues(ArrayList<Tortue> listeTortuesConnues) {
        this.listeTortuesConnues = listeTortuesConnues;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return vrai si démarqué
     */
    public boolean isDemarque() {
        return demarque;
    }

    /**
     *
     * @param demarque vrai si démarqué
     */
    public void setDemarque(boolean demarque) {
        this.demarque = demarque;
    }

    /**
     * Met à jour la situation de démarquage de la tortue une tortue est
     * démarquée si aucune tortue adverse se trouve dans son rayon
     *
     * @param rayon rayon en pixels
     */
    public void majDemarque(int rayon) {
        for (Tortue tortue : this.listeTortuesConnues) {
            if (tortue instanceof TortueAmelioree) {
                TortueAmelioree t = (TortueAmelioree) tortue;
                if (this.calculerDistance(t) <= rayon) {
                    this.demarque = false;
                    return;
                }
            }
        }
        this.demarque = true;
    }

    /**
     * Ajoute la tortue à la liste des tortues connues puis notifie ses
     * observeurs
     *
     * @param t la tortue à ajouter
     */
    public void ajouterTortueConnue(Tortue t) {
        this.listeTortuesConnues.add(t);
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @param t la tortue à supprimer
     */
    public void supprimerTortueConnue(Tortue t) {
        this.listeTortuesConnues.remove(t);
    }

    /**
     *
     * @param t la tortue
     * @return la distance entre cette tortue et t
     */
    public double calculerDistance(Tortue t) {
        return Math.sqrt((t.getX() - this.getX()) * (t.getX() - this.getX()) + (t.getY() - this.getY()) * (t.getY() - this.getY()));
    }

    /**
     * Déplace cette tortue selon la stratégie de Positionnement
     */
    @Override
    public void deplacer(ArrayList<Integer> e) {
        this.strategie.getPosition().deplacer(this, e);
    }

    /**
     * Frappe la balle selon la stratégie de Coopération
     */
    public void frapperBalle() {
        this.strategie.getCooperation().frapperBalle(this);
    }

    /**
     *
     * @return le nom de la tortue
     */
    @Override
    public String toString() {
        return this.nom;
    }
}
