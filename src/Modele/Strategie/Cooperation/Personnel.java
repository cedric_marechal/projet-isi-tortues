package Modele.Strategie.Cooperation;

import Modele.PartieDeFoot;
import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;

/**
 * Stratégie de jeu personnel
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Personnel implements Cooperation {

    private final int DISTANCE_BALLE_MIN = 10;

    /**
     * Définit comment frapper la balle. La balle doit être assez proche
     * (DISTANCE_BALLE_MIN pixels) pour pouvoir être frappée. Se dirige en
     * direction des cages adverses.
     *
     * @param tortue la tortue qui frappe la balle
     */
    @Override
    public void frapperBalle(TortueAmelioree tortue) {
        TortueBalle tortueBalleProche = null;

        for (Tortue t : tortue.getListeTortuesConnues()) {
            double distanceTortue = tortue.calculerDistance(t);
            // Recherche de la tortue alliée et de la balle les plus proches
            if (distanceTortue <= tortue.getVision()) {
                if (t instanceof TortueBalle) {
                    // Si une balle est assez proche, elle sera frappée
                    if (distanceTortue <= DISTANCE_BALLE_MIN) {
                        tortueBalleProche = (TortueBalle) t;
                    }
                }
            }
        }

        // S'il y a une balle assez proche
        if (tortueBalleProche != null) {

            // Si le joueur est seul, tire en direction des cages
            int angle = 0;
            int xCible;
            if (tortue.getEquipe() % 2 == 1) {
                xCible = PartieDeFoot.getLongeurTerrain() - (PartieDeFoot.getMarge() / 4);
            } else {
                xCible = (PartieDeFoot.getMarge() / 2) + (PartieDeFoot.getMarge() / 4);
            }
            int yCible = PartieDeFoot.getLargeurTerrain() / 2;
            angle = (int) Math.toDegrees(Math.atan2(yCible - tortueBalleProche.getY(), xCible - tortueBalleProche.getX()));
            angle = (((angle % 360) + 360) % 360);

            tortue.setDir(angle);
            // Frappe la balle
            tortueBalleProche.pousser(tortue.getDir(), tortue.getForce());
        }

    }

    /**
     *
     * @return le nom de la stratégie
     */
    @Override
    public String toString() {
        return "Personnel";
    }
}
