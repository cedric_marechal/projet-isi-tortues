package Modele.Strategie.Cooperation;

import Modele.PartieDeFoot;
import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;

/**
 * Stratégie de jeu collectif
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Collectif implements Cooperation {

    private final int DISTANCE_BALLE_MIN = 10;

    /**
     * Définit comment frapper la balle. La balle doit être assez proche
     * (DISTANCE_BALLE_MIN pixels) pour pouvoir être frappée. Si une tortue
     * alliée est proche et démarquée, lui passe la balle, sinon, se dirige en
     * direction des cages adverses.
     * Essaye de contourner les adversaires proches.
     *
     * @param tortue la tortue qui frappe la balle
     */
    @Override
    public void frapperBalle(TortueAmelioree tortue) {

        double distanceAmieMin = 9999999;
        double distanceEnnemieMin = 9999999;
        TortueBalle tortueBalleProche = null;
        TortueAmelioree tortueAmieProche = null;
        TortueAmelioree tortueEnnemieProche = null;

        for (Tortue t : tortue.getListeTortuesConnues()) {
            double distanceTortue = tortue.calculerDistance(t);
            // Recherche de la tortue alliée et de la balle les plus proches
            if (distanceTortue <= tortue.getVision()) {
                if (t instanceof TortueAmelioree) {
                    TortueAmelioree tortueProche = (TortueAmelioree) t;
                    if (tortue.getEquipe() == tortueProche.getEquipe() && tortueProche.isDemarque() && distanceTortue < distanceAmieMin) {
                        distanceAmieMin = distanceTortue;
                        tortueAmieProche = tortueProche;
                    } else if (tortue.getEquipe() != tortueProche.getEquipe() && distanceTortue < distanceEnnemieMin) {
                        distanceEnnemieMin = distanceTortue;
                        tortueEnnemieProche = tortueProche;
                    }
                } else if (t instanceof TortueBalle) {
                    // Si une balle est assez proche, elle sera frappée
                    if (distanceTortue <= DISTANCE_BALLE_MIN) {
                        tortueBalleProche = (TortueBalle) t;
                    }
                }
            }
        }

        // S'il y a une balle assez proche
        if (tortueBalleProche != null) {
            // Si une tortue alliée est proche, tirera dans sa direction
            if (tortueAmieProche != null) {
                // Angle de tir pour faire une passe de tortue à tortueAmieProche
                int angle = (int) Math.toDegrees(Math.atan2(tortueAmieProche.getY() - tortue.getY(), tortueAmieProche.getX() - tortue.getX()));
                if (angle < 0) {
                    angle += 360;
                }
                tortueBalleProche.pousser(angle, tortue.getForce());
            } else {
                // Si pas d'amis, tire devant pour faire avancer la balle (Jeu solo)          

                // Si le joueur est seul, tire en direction des cages
                int angle = 0;
                // if (tortueEnnemieProche == null) {
                int xCible;
                if (tortue.getEquipe() % 2 == 1) {
                    xCible = PartieDeFoot.getLongeurTerrain() - (PartieDeFoot.getMarge() / 4);
                } else {
                    xCible = (PartieDeFoot.getMarge() / 2) + (PartieDeFoot.getMarge() / 4);
                }
                int yCible = PartieDeFoot.getLargeurTerrain() / 2;
                angle = (int) Math.toDegrees(Math.atan2(yCible - tortueBalleProche.getY(), xCible - tortueBalleProche.getX()));
                angle = (((angle % 360) + 360) % 360);
            /*}  else {
            // Un joueur ennemi est proche, on tente de le contourner
            int angleTortues = (int) Math.toDegrees(Math.atan2(tortueEnnemieProche.getY() - tortueBalleProche.getY(), tortueEnnemieProche.getX() - tortueBalleProche.getX()));
            angleTortues = (((angleTortues % 360) + 360) % 360);

            // Tire en direction NO, NE, SE, SO + ajout d'un paramètre aléatoire
            Random rand = new Random();
            int angleRand = rand.nextInt(90);
            angleRand -= 45;
            if (tortue.getEquipe() % 2 == 1) {
                // Ennemi devant soi
                if (angleTortues < 90 || angleTortues > 270) {
                    if (angleTortues < 90) {
                        // Tire vers le haut
                        angle = 360 - 45 + angleRand;
                    } else {
                        // Tire vers le bas
                        angle = 45 + angleRand;
                    }
                }
            } else {
                // Ennemi devant soi
                if (angleTortues > 90 && angleTortues < 270) {
                    if (angleTortues < 180) {
                        // Tire vers le haut
                        angle = 180 + 45 + angleRand;
                    } else {
                        // Tire vers le bas
                        angle = 180 - 45 + angleRand;
                    }
                }
            }
        }*/

        tortue.setDir(angle);
        // Frappe la balle
        tortueBalleProche.pousser(tortue.getDir(), tortue.getForce());
    }
}
}

    /**
     *
     * @return le nom de la stratégie
     */
    @Override
        public String toString() {
        return "Collectif";
    }
}
