package Modele.Strategie.Cooperation;

import Modele.TortueAmelioree;

/**
 * Stratégie de jeu coopératif
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public interface Cooperation {

    /**
     *
     * @param tortue la tortue qui frappe la balle
     */
    public void frapperBalle(TortueAmelioree tortue);
}
