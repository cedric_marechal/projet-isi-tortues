package Modele.Strategie.Cooperation;

import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class StrategieJeuDeBalle implements Cooperation {

    private final int DISTANCE_BALLE_MIN = 10;

    /**
     * Définit comment frapper la balle. La balle doit être assez proche
     * (DISTANCE_BALLE_MIN pixels) pour pouvoir être frappée. Si une tortue
     * alliée est proche, lui passe la balle
     *
     * @param tortue la tortue qui frappe la balle
     */
    @Override
    public void frapperBalle(TortueAmelioree tortue) {

        TortueBalle tortueBalleProche = null;
        TortueAmelioree tortueAmieProche = null;

        for (Tortue t : tortue.getListeTortuesConnues()) {
            double distanceTortue = tortue.calculerDistance(t);
            // Recherche de la tortue alliée et de la balle les plus proches
            if (distanceTortue <= tortue.getVision()) {
                if (t instanceof TortueAmelioree) {
                    TortueAmelioree tortueProche = (TortueAmelioree) t;
                    tortueAmieProche = tortueProche;
                } else if (t instanceof TortueBalle) {
                    // Si une balle est assez proche, elle sera frappée
                    if (distanceTortue <= DISTANCE_BALLE_MIN) {
                        tortueBalleProche = (TortueBalle) t;
                    }
                }
            }
        }

        // S'il y a une balle assez proche
        if (tortueBalleProche != null) {
            // Si une tortue alliée est proche, tirera dans sa direction
            if (tortueAmieProche != null) {
                // Angle de tir pour faire une passe de tortue à tortueAmieProche
                int angle = (int) Math.toDegrees(Math.atan2(tortueAmieProche.getY() - tortue.getY(), tortueAmieProche.getX() - tortue.getX()));
                if (angle < 0) {
                    angle += 360;
                }
                tortueBalleProche.pousser(angle, tortue.getForce());
            } else {
                tortueBalleProche.pousser(tortue.getDir(), tortue.getForce());
            }
        }
    }
}
