package Modele.Strategie.Cooperation;

import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class TirJoueur implements Cooperation {

    private final int DISTANCE_BALLE_MIN = 10;

    /**
     * Définit comment frapper la balle. La balle doit être assez proche
     * (DISTANCE_BALLE_MIN pixels) pour pouvoir être frappée. Frappe tout droit
     *
     * @param tortue la tortue qui frappe la balle
     */
    @Override
    public void frapperBalle(TortueAmelioree tortue) {
        TortueBalle tortueBalleProche = null;

        for (Tortue t : tortue.getListeTortuesConnues()) {
            double distanceTortue = tortue.calculerDistance(t);
            // Recherche de la tortue alliée et de la balle les plus proches
            if (distanceTortue <= tortue.getVision()) {
                if (t instanceof TortueBalle) {
                    // Si une balle est assez proche, elle sera frappée
                    if (distanceTortue <= DISTANCE_BALLE_MIN) {
                        tortueBalleProche = (TortueBalle) t;
                    }
                }
            }
        }

        // S'il y a une balle assez proche
        if (tortueBalleProche != null) {
            // Frappe la balle
            tortueBalleProche.pousser(tortue.getDir(), tortue.getForce());
        }

    }

    /**
     *
     * @return le nom de la stratégie
     */
    @Override
    public String toString() {
        return "Joueur";
    }
}
