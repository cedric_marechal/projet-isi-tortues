package Modele.Strategie;

import Modele.Strategie.Cooperation.Cooperation;
import Modele.Strategie.Position.Position;

/**
 * Une stratégie alliant positionnnement et coopération
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Strategie {

    private Position position;
    private Cooperation cooperation;

    /**
     *
     * @param position la stratégie de positionnement
     * @param cooperation la stratégie de coopération
     */
    public Strategie(Position position, Cooperation cooperation) {
        this.position = position;
        this.cooperation = cooperation;
    }

    /**
     *
     * @return la stratégie de positionnement
     */
    public Position getPosition() {
        return position;
    }

    /**
     *
     * @param position la stratégie de positionnement
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     *
     * @return la stratégie de coopération
     */
    public Cooperation getCooperation() {
        return cooperation;
    }

    /**
     *
     * @param cooperation la stratégie de coopération
     */
    public void setCooperation(Cooperation cooperation) {
        this.cooperation = cooperation;
    }
}
