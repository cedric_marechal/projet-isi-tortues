package Modele.Strategie.Position;

import Modele.PartieDeFoot;
import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Gardien implements Position {
    
    /**
     * Se dirige vers la balle si elle se trouve dans la surface de réparation
     * sinon, se repositionne sur sa position initiale.
     *
     * Reste dans la surface de réparation
     * 
     * @param tortue la tortue à déplacer
     * @param e la liste des touches du clavier pressées
     */
    @Override
    public void deplacer(TortueAmelioree tortue, ArrayList<Integer> e) {
        
        // Zone limite pour la surface de réparation
        Point limiteHautGauche = new Point();
        Point limiteBasDroit = new Point();
        if(tortue.getEquipe()==1){
            limiteHautGauche = new Point(PartieDeFoot.getMarge(),(PartieDeFoot.getLargeurTerrain()/2)-(PartieDeFoot.getLongueurSurfaceReparation()/2));
            limiteBasDroit = new Point(PartieDeFoot.getMarge()+PartieDeFoot.getLargeurSurfaceReparation(),(PartieDeFoot.getLargeurTerrain()/2)+(PartieDeFoot.getLongueurSurfaceReparation()/2));
        } else if(tortue.getEquipe()==2){
            limiteHautGauche = new Point(PartieDeFoot.getLongeurTerrain()-PartieDeFoot.getLargeurSurfaceReparation()-PartieDeFoot.getMarge(),(PartieDeFoot.getLargeurTerrain()/2)-(PartieDeFoot.getLongueurSurfaceReparation()/2));
            limiteBasDroit = new Point(PartieDeFoot.getLongeurTerrain()-PartieDeFoot.getLargeurSurfaceReparation()-PartieDeFoot.getMarge()+PartieDeFoot.getLargeurSurfaceReparation(),(PartieDeFoot.getLargeurTerrain()/2)+(PartieDeFoot.getLongueurSurfaceReparation()/2));
        }
        
        // Récupére la liste des balles dans la zone de surface de réparation
        ArrayList<TortueBalle> listBalles = new ArrayList();
        for (Tortue t : tortue.getListeTortuesConnues()) {
            boolean balleZoneDef = false;
            if (tortue.getEquipe() == 1 && t.getX() < limiteBasDroit.x && t.getY() < limiteBasDroit.y && t.getY() > limiteHautGauche.y) {
                balleZoneDef = true;
            } else if (tortue.getEquipe() == 2 && t.getX() > limiteHautGauche.x && t.getY() < limiteBasDroit.y && t.getY() > limiteHautGauche.y) {
                balleZoneDef = true;
            }

            if (balleZoneDef && t instanceof TortueBalle) {
                listBalles.add((TortueBalle) t);
            }
        }
        
        // Détermine la direction
        if (!listBalles.isEmpty()) {
            TortueBalle b = listBalles.get(0);
            int angle = (int) Math.toDegrees(Math.atan2(b.getY() - tortue.getY(), b.getX() - tortue.getX()));
            if (angle < 0) {
                angle += 360;
            }
            tortue.setDir(angle);
            // Avance dans sa zone
            tortue.avancer(1, limiteHautGauche, limiteBasDroit);
        } else {
            // Retour à sa position initiale
            if (tortue.getyInitial() != tortue.getY() && tortue.getxInitial() != tortue.getX()) {
                int angle = (int) Math.toDegrees(Math.atan2(tortue.getyInitial() - tortue.getY(), tortue.getxInitial() - tortue.getX()));
                if (angle < 0) {
                    angle += 360;
                }
                tortue.setDir(angle);
                // Avance dans sa zone
                tortue.avancer(1);
            } // Sinon, on ne bouge pas
        }
    }
    
    /**
     * 
     * @return le nom de la stratégie
     */
    @Override
    public String toString(){
        return "Gardien";
    }
}
