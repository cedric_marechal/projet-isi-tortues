package Modele.Strategie.Position;

import Modele.PartieDeFoot;
import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Milieu implements Position {

    /**
     * Se dirige vers la balle si elle se trouve dans le champ de vision et si
     * une tortue alliée n'est pas déjà en train de se diriger vers la balle;
     * sinon, se déplace aléatoirement.
     *
     * Reste dans sa zone de milieu
     *
     * @param tortue la tortue à déplacer
     * @param e la liste des touches du clavier pressées
     */
    @Override
    public void deplacer(TortueAmelioree tortue, ArrayList<Integer> e) {
        ArrayList<TortueBalle> listBalles = new ArrayList();
        for (Tortue t : tortue.getListeTortuesConnues()) {
            if (t instanceof TortueBalle) {
                listBalles.add((TortueBalle) t);
            }
        }
        double distanceMin = 99999999;
        if (!listBalles.isEmpty()) {
            for (TortueBalle b : listBalles) {
                for (Tortue t : tortue.getListeTortuesConnues()) {
                    if (t instanceof TortueAmelioree) {
                        TortueAmelioree ta = (TortueAmelioree) t;
                        double distance = ta.calculerDistance(b);
                        if (tortue.getEquipe() == ta.getEquipe() && distance < distanceMin) {
                            distanceMin = distance;
                        }
                    }
                }
                double DistanceBalle = tortue.calculerDistance(b);
                if (DistanceBalle < distanceMin) {
                    int angle = (int) Math.toDegrees(Math.atan2(b.getY() - tortue.getY(), b.getX() - tortue.getX()));
                    if (angle < 0) {
                        angle += 360;
                    }
                    tortue.setDir(angle);
                } else {
                    Random rand = new Random();
                    int mouvement = rand.nextInt(3);
                    if (mouvement == 1) {
                        tortue.gauche(1);
                    }
                    if (mouvement == 2) {
                        tortue.droite(1);
                    }
                }
            }
        }

        Point limiteHautGauche = new Point();
        Point limiteBasDroit = new Point();
        if (tortue.getEquipe() == 1) {
            limiteHautGauche = new Point(PartieDeFoot.getLongeurTerrain() / 6, 0);
            limiteBasDroit = new Point(PartieDeFoot.getLongeurTerrain(), PartieDeFoot.getLargeurTerrain());
        } else if (tortue.getEquipe() == 2) {
            limiteHautGauche = new Point(0, 0);
            limiteBasDroit = new Point(5 * PartieDeFoot.getLongeurTerrain() / 6, PartieDeFoot.getLargeurTerrain());
        }
        tortue.avancer(1, limiteHautGauche, limiteBasDroit);
    }

    /**
     *
     * @return le nom de la stratégie
     */
    @Override
    public String toString() {
        return "Milieu";
    }
}
