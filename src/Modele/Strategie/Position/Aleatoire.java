package Modele.Strategie.Position;

import Modele.TortueAmelioree;
import java.util.ArrayList;
import java.util.Random;

/**
 * Stratégie de positionnement aléatoire
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Aleatoire implements Position {

    /**
     * Se déplace aléatoirement
     * @param tortue la tortue à déplacer
     * @param e la liste des touches du clavier pressées
     */
    @Override
    public void deplacer(TortueAmelioree tortue, ArrayList<Integer> e) {

        Random rand = new Random();
        int mouvement = rand.nextInt(3);
        if (mouvement == 1) {
            tortue.gauche(1);
        }
        if (mouvement == 2) {
            tortue.droite(1);
        }
        tortue.avancer(1);
    }
}
