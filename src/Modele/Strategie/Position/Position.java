package Modele.Strategie.Position;

import Modele.TortueAmelioree;
import java.util.ArrayList;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public interface Position {

    /**
     * Définit le déplacement
     * @param t la tortue à déplacer
     * @param e la liste des touches du clavier pressées
     */
    public void deplacer(TortueAmelioree t, ArrayList<Integer> e);
}
