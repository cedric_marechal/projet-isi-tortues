package Modele.Strategie.Position;

import Modele.TortueAmelioree;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Joueur2 implements Position {

    /**
     * Déplace selon les touches pressées (Z, S, Q, D)
     *
     * @param tortue la tortue à déplacer
     * @param listeTouchesPressees la liste des touches du clavier pressées
     */
    @Override
    public void deplacer(TortueAmelioree tortue, ArrayList<Integer> listeTouchesPressees) {
        if (!listeTouchesPressees.isEmpty()) {
            // Directions diagonales
            if (listeTouchesPressees.contains(KeyEvent.VK_Z) && listeTouchesPressees.contains(KeyEvent.VK_D)) {
                tortue.setDir(315);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_D) && listeTouchesPressees.contains(KeyEvent.VK_S)) {
                tortue.setDir(45);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_S) && listeTouchesPressees.contains(KeyEvent.VK_Q)) {
                tortue.setDir(135);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_Q) && listeTouchesPressees.contains(KeyEvent.VK_Z)) {
                tortue.setDir(225);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_Z)) {
                tortue.setDir(270);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_D)) {
                tortue.setDir(0);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_S)) {
                tortue.setDir(90);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_Q)) {
                tortue.setDir(180);
                tortue.avancer(1);
            }
        }
    }

    /**
     *
     * @return le nom de la stratégie
     */
    @Override
    public String toString() {
        return "Joueur2";
    }
}
