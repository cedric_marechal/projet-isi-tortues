package Modele.Strategie.Position;

import Modele.TortueAmelioree;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Joueur1 implements Position {

    /**
     * Déplace selon les touches pressées (HAUT, BAS, GAUCHE, DROITE)
     *
     * @param tortue la tortue à déplacer
     * @param listeTouchesPressees la liste des touches du clavier pressées
     */
    @Override
    public void deplacer(TortueAmelioree tortue, ArrayList<Integer> listeTouchesPressees) {
        if (!listeTouchesPressees.isEmpty()) {
            // Directions diagonales
            if (listeTouchesPressees.contains(KeyEvent.VK_UP) && listeTouchesPressees.contains(KeyEvent.VK_RIGHT)) {
                tortue.setDir(315);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_RIGHT) && listeTouchesPressees.contains(KeyEvent.VK_DOWN)) {
                tortue.setDir(45);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_DOWN) && listeTouchesPressees.contains(KeyEvent.VK_LEFT)) {
                tortue.setDir(135);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_LEFT) && listeTouchesPressees.contains(KeyEvent.VK_UP)) {
                tortue.setDir(225);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_UP)) {
                tortue.setDir(270);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_RIGHT)) {
                tortue.setDir(0);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_DOWN)) {
                tortue.setDir(90);
                tortue.avancer(1);
            } else if (listeTouchesPressees.contains(KeyEvent.VK_LEFT)) {
                tortue.setDir(180);
                tortue.avancer(1);
            }
        }
    }

    /**
     *
     * @return le nom de la stratégie
     */
    @Override
    public String toString() {
        return "Joueur1";
    }
}
