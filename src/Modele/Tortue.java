package Modele;

import java.awt.*;
import java.util.*;

/**
 * LEs données de la tortue
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public abstract class Tortue extends Observable {

    protected int x, y;	// Coordonnees de la tortue
    private int xInitial, yInitial;
    protected int dir;	// Direction de la tortue (angle en degres)
    protected Color coul;
    private Polygon arrow;
    private int vitesse;

    /**
     *
     * @return le type de dessin
     */
    public Polygon getArrow() {
        return arrow;
    }

    /**
     *
     * @param arrow le type de dessin
     */
    public void setArrow(Polygon arrow) {
        this.arrow = arrow;
    }

    /**
     *
     * @return la position X en pixels
     */
    public int getX() {
        return x;
    }

    /**
     * Met à jour la position X puis notifie ses observeurs
     *
     * @param x la position X en pixels
     */
    public void setX(int x) {
        this.x = x;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la position Y en pixels
     */
    public int getY() {
        return y;
    }

    /**
     * Met à jour la position Y puis notifie ses observeurs
     *
     * @param y la position Y en pixels
     */
    public void setY(int y) {
        this.y = y;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la position X initiale en pixels
     */
    public int getxInitial() {
        return xInitial;
    }

    /**
     *
     * @param xInitial la position X initiale en pixels
     */
    public void setxInitial(int xInitial) {
        this.xInitial = xInitial;
    }

    /**
     *
     * @return la position Y initiale en pixels
     */
    public int getyInitial() {
        return yInitial;
    }

    /**
     *
     * @param yInitial la position Y initiale en pixels
     */
    public void setyInitial(int yInitial) {
        this.yInitial = yInitial;
    }

    /**
     *
     * @return la direction en degrés de la tortue
     */
    public int getDir() {
        return dir;
    }

    /**
     * Met à jour la direction puis notifie ses observeurs
     *
     * @param dir la direction en degrés de la tortue
     */
    public void setDir(int dir) {
        this.dir = dir;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la couleur de la tortue
     */
    public Color getCoul() {
        return coul;
    }

    /**
     * Met à jour la couleur puis notifie ses observeurs
     *
     * @param coul la couleur de la tortue
     */
    public void setCoul(Color coul) {
        this.coul = coul;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return la vitesse de la tortue en FPS
     */
    public int getVitesse() {
        return vitesse;
    }

    /**
     * Vérifie si la vitesse est > 0 et l'affecte
     *
     * @param vitesse la vitesse de la tortue en FPS
     */
    public void setVitesse(int vitesse) {
        if (vitesse <= 0) {
            this.vitesse = 0;
        } else {
            this.vitesse = vitesse;
        }
    }

    /**
     *
     * @param x la position X en pixels
     * @param y la position Y en pixels
     * @param dir la direction en degrés de la tortue
     * @param vitesse la vitesse de la tortue en FPS
     * @param coul la couleur de la tortue
     */
    public Tortue(int x, int y, int dir, int vitesse, Color coul) {
        this.x = x;
        this.y = y;
        this.xInitial = x;
        this.yInitial = y;
        this.dir = dir;
        this.vitesse = vitesse;
        this.coul = coul;
    }

    /**
     * Réinitialise la tortue puis notifie ses observeurs
     */
    public void reset() {
        // on initialise la position de la tortue
        x = 0;
        y = 0;
        dir = -90;
        coul = Color.BLACK;
        setChanged();
        notifyObservers();
    }

    /**
     * Met à jour la position X et Y puis notifie ses observeurs
     *
     * @param newX la nouvelle position X
     * @param newY la nouvelle position Y
     */
    public void setPosition(int newX, int newY) {
        x = newX;
        y = newY;
        setChanged();
        notifyObservers();
    }

    /**
     * Avance la tortue de dist pixels. Si on atteint les bornes du terrain, on
     * retourne en arrière
     *
     * @param dist le nombre de pixels à avancer
     */
    public void avancer(int dist) {
        int newX = (int) Math.round(this.getX() + dist * Math.cos(Constante.ratioDegRad * this.getDir()));
        int newY = (int) Math.round(this.getY() + dist * Math.sin(Constante.ratioDegRad * this.getDir()));
        if (newX <= 10 || newY <= 10 || newX >= PartieDeFoot.getLongeurTerrain() - 10 || newY >= PartieDeFoot.getLargeurTerrain() - 10) {
            this.setDir(this.getDir() + 180);
            newX = (int) Math.round(this.getX() + dist * Math.cos(Constante.ratioDegRad * this.getDir()));
            newY = (int) Math.round(this.getY() + dist * Math.sin(Constante.ratioDegRad * this.getDir()));
        }
        this.setX(newX);
        this.setY(newY);
    }

    /**
     * Avance la tortue de dist pixels. Si on atteint les bornes du terrain, on
     * retourne en arrière. Si on est en dehors du rectangle défini par
     * limiteHautGauche et limiteBasDroit, on se dirige au centre du rectangle
     *
     * @param dist le nombre de pixels à avancer
     * @param limiteHautGauche le point de la limite haut gauche du rectangle
     * @param limiteBasDroit le point de la limite bas droit du rectangle
     */
    public void avancer(int dist, Point limiteHautGauche, Point limiteBasDroit) {
        int newX = (int) Math.round(this.getX() + dist * Math.cos(Constante.ratioDegRad * this.getDir()));
        int newY = (int) Math.round(this.getY() + dist * Math.sin(Constante.ratioDegRad * this.getDir()));
        if (newX <= PartieDeFoot.getMarge() || newY <= PartieDeFoot.getMarge() || newX >= PartieDeFoot.getLongeurTerrain() - PartieDeFoot.getMarge() || newY >= PartieDeFoot.getLargeurTerrain() - PartieDeFoot.getMarge()
                || newX < limiteHautGauche.x || newX > limiteBasDroit.x || newY < limiteHautGauche.y || newY > limiteBasDroit.y) {
            int centreXZone = limiteHautGauche.x + (limiteBasDroit.x - limiteHautGauche.x) / 2;
            int centreYZone = limiteHautGauche.y + (limiteBasDroit.y - limiteHautGauche.y) / 2;
            int angle = (int) Math.toDegrees(Math.atan2(centreYZone - this.getY(), centreXZone - this.getX()));
            this.setDir(angle);
            newX = (int) Math.round(this.getX() + dist * Math.cos(Constante.ratioDegRad * this.getDir()));
            newY = (int) Math.round(this.getY() + dist * Math.sin(Constante.ratioDegRad * this.getDir()));
        }
        this.setX(newX);
        this.setY(newY);
    }

    /**
     * Tourne la tortue à droite
     *
     * @param ang l'angle à ajouter en degrés
     */
    public void droite(int ang) {
        this.setDir((this.getDir() + ang) % 360);
    }

    /**
     * Tourne la tortue à gauche
     *
     * @param ang l'angle à ajouter en degrés
     */
    public void gauche(int ang) {
        this.setDir((this.getDir() - ang) % 360);
    }

    /**
     * Fonction de déplacement
     */
    public abstract void deplacer(ArrayList<Integer> e);
}
