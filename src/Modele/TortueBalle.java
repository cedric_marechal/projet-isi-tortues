package Modele;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class TortueBalle extends Tortue {

    /**
     *
     * @param x la position X en pixels
     * @param y la position Y en pixels
     * @param dir la direction en degrés de la tortue
     * @param vitesse la vitesse de la tortue en FPS
     * @param coul la couleur de la tortue
     */
    public TortueBalle(int x, int y, int dir, int vitesse, Color coul) {
        super(x, y, dir, vitesse, coul);
    }

    /**
     * Pousse la balle selon une vitesse et une direction
     *
     * @param dir la direction en degrés de la tortue
     * @param vitesse la vitesse de la tortue en FPS
     */
    public void pousser(int dir, int vitesse) {
        this.setDir(dir);
        this.setVitesse(vitesse);
    }

    /**
     * Vérifie si le ballon se situe dans un rectangle
     *
     * @param p le point haut gauche du rectangle
     * @return vrai si le ballon est dans le rectangle
     */
    public boolean isInRectangle(Point p) {
        if (this.x >= p.x && this.x <= p.x + PartieDeFoot.getMarge() / 2 && this.y >= p.y && this.y <= p.y + PartieDeFoot.getHauteurCage()) {
            return true;
        }
        return false;
    }

    /**
     * Fait avancer la balle si elle possède une vitesse. Décrémente la vitesse
     * au fil du temps.
     */
    @Override
    public void deplacer(ArrayList<Integer> e) {
        if (this.getVitesse() > 0) {
            this.avancer(1);
            this.setVitesse((this.getVitesse()) - 1);
        }
    }
}
