/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import java.util.Observable;

/**
 *
 * @author Cédric
 */
public class PartieDeTennis extends Observable{
    
    private int pointJoueur1;
    private int pointJoueur2;
    private int jeuJoueur1;
    private int jeuJoueur2;
    private int setJoueur1;
    private int setJoueur2;
    private static int largeurTerrain = 400;
    private static int longeurTerrain = 900;
    private static int largeurCouloir = 50;
    private static int largeurCarreDeService = 180;
    private static int marge = 20;
    
    public PartieDeTennis(){
        this.pointJoueur1 = 0;
        this.pointJoueur2 = 0;
        this.jeuJoueur1 = 0;
        this.jeuJoueur2 = 0;
        this.setJoueur1 = 0;
        this.setJoueur2 = 0;
    }

    public int getPointJoueur1() {
        return pointJoueur1;
    }

    public void setPointJoueur1(int pointJoueur1) {
        this.pointJoueur1 = pointJoueur1;
        setChanged();
        notifyObservers();
    }

    public int getPointJoueur2() {
        return pointJoueur2;
    }

    public void setPointJoueur2(int pointJoueur2) {
        this.pointJoueur2 = pointJoueur2;
        setChanged();
        notifyObservers();
    }

    public int getJeuJoueur1() {
        return jeuJoueur1;
    }

    public void setJeuJoueur1(int jeuJoueur1) {
        this.jeuJoueur1 = jeuJoueur1;
        setChanged();
        notifyObservers();
    }

    public int getJeuJoueur2() {
        return jeuJoueur2;
    }

    public void setJeuJoueur2(int jeuJoueur2) {
        this.jeuJoueur2 = jeuJoueur2;
        setChanged();
        notifyObservers();
    }

    public int getSetJoueur1() {
        return setJoueur1;
    }

    public void setSetJoueur1(int setJoueur1) {
        this.setJoueur1 = setJoueur1;
        setChanged();
        notifyObservers();
    }

    public int getSetJoueur2() {
        return setJoueur2;
    }

    public void setSetJoueur2(int setJoueur2) {
        this.setJoueur2 = setJoueur2;
        setChanged();
        notifyObservers();
    }

    public void updateScoreJoueur1(){
        if(this.pointJoueur1>=40){
            if(this.jeuJoueur1+1>=6){
                this.setJoueur1++;
            }
            else{
                this.jeuJoueur1++;
            }
        }
        else{
            if(this.pointJoueur1<=15){
                this.pointJoueur1 = this.pointJoueur1 + 15;
            }
            else{
                this.pointJoueur1 = this.jeuJoueur1 + 10;
            }
        }
        setChanged();
        notifyObservers();   
    }
    
    public void updateScoreJoueur2(){
        if(this.pointJoueur2>=40){
            if(this.jeuJoueur2+1>=6){
                this.setJoueur2++;
            }
            else{
                this.jeuJoueur2++;
            }
        }
        else{
            if(this.pointJoueur2<=15){
                this.pointJoueur2 = this.pointJoueur2 + 15;
            }
            else{
                this.pointJoueur2 = this.jeuJoueur2 + 10;
            }
        }
        setChanged();
        notifyObservers();   
    }
    
    
    public static int getLargeurTerrain() {
        return largeurTerrain;
    }

    public static void setLargeurTerrain(int largeurTerrain) {
        PartieDeTennis.largeurTerrain = largeurTerrain;
    }

    public static int getLongeurTerrain() {
        return longeurTerrain;
    }

    public static void setLongeurTerrain(int longeurTerrain) {
        PartieDeTennis.longeurTerrain = longeurTerrain;
    }

    public static int getLargeurCouloir() {
        return largeurCouloir;
    }

    public static void setLargeurCouloir(int largeurCouloir) {
        PartieDeTennis.largeurCouloir = largeurCouloir;
    }

    public static int getLargeurCarreDeService() {
        return largeurCarreDeService;
    }

    public static void setLargeurCarreDeService(int largeurCarreDeService) {
        PartieDeTennis.largeurCarreDeService = largeurCarreDeService;
    }

    public static int getMarge() {
        return marge;
    }

    public static void setMarge(int marge) {
        PartieDeTennis.marge = marge;
    }

   

    
    
}
