package Modele;

import java.util.Observable;

/**
 * Les données du jeu de foot
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class PartieDeFoot extends Observable {

    private int scoreEquipe1;
    private int scoreEquipe2;
    private static int largeurTerrain = 580;
    private static int longeurTerrain = 900;
    private static int largeurSurfaceReparation = 80;
    private static int longueurSurfaceReparation = 180;
    private static int rayonMilieuTerrain = 80;
    private static int marge = 20;
    private static int hauteurCage = longueurSurfaceReparation / 4;

    /**
     * Initialisation des scores à 0
     */
    public PartieDeFoot() {
        this.scoreEquipe1 = 0;
        this.scoreEquipe2 = 0;
    }

    /**
     *
     * @return la marge de la zone de touche en pixels
     */
    public static int getMarge() {
        return marge;
    }

    /**
     *
     * @return la hauteur de la zone de cage en pixels
     */
    public static int getHauteurCage() {
        return hauteurCage;
    }

    /**
     *
     * @param hauteurCage la hauteur de la zone de cage en pixels
     */
    public static void setHauteurCage(int hauteurCage) {
        PartieDeFoot.hauteurCage = hauteurCage;
    }

    /**
     *
     * @param marge la marge de la zone de touche en pixels
     */
    public static void setMarge(int marge) {
        PartieDeFoot.marge = marge;
    }

    /**
     *
     * @return le rayon du cercle central en pixels
     */
    public static int getRayonMilieuTerrain() {
        return rayonMilieuTerrain;
    }

    /**
     *
     * @param rayonMilieuTerrain le rayon du cercle central en pixels
     */
    public static void setRayonMilieuTerrain(int rayonMilieuTerrain) {
        PartieDeFoot.rayonMilieuTerrain = rayonMilieuTerrain;
    }

    /**
     *
     * @return la largeur de la surface de réparation en pixels
     */
    public static int getLargeurSurfaceReparation() {
        return largeurSurfaceReparation;
    }

    /**
     *
     * @param largeurSurfaceReparation la largeur de la surface de réparation en
     * pixels
     */
    public static void setLargeurSurfaceReparation(int largeurSurfaceReparation) {
        PartieDeFoot.largeurSurfaceReparation = largeurSurfaceReparation;
    }

    /**
     *
     * @return la longueur de la surface de réparation en pixels
     */
    public static int getLongueurSurfaceReparation() {
        return longueurSurfaceReparation;
    }

    /**
     *
     * @param longueurSurfaceReparation la longueur de la surface de réparation
     * en pixels
     */
    public static void setLongueurSurfaceReparation(int longueurSurfaceReparation) {
        PartieDeFoot.longueurSurfaceReparation = longueurSurfaceReparation;
    }

    /**
     *
     * @return la largeur du terrain en pixels
     */
    public static int getLargeurTerrain() {
        return largeurTerrain;
    }

    /**
     *
     * @param largeurTerrain la largeur du terrain en pixels
     */
    public static void setLargeurTerrain(int largeurTerrain) {
        PartieDeFoot.largeurTerrain = largeurTerrain;
    }

    /**
     *
     * @return la longueur du terrain en pixels
     */
    public static int getLongeurTerrain() {
        return longeurTerrain;
    }

    /**
     *
     * @param longeurTerrain la longueur du terrain en pixels
     */
    public static void setLongeurTerrain(int longeurTerrain) {
        PartieDeFoot.longeurTerrain = longeurTerrain;
    }

    /**
     *
     * @return le score de l'équipe 1
     */
    public int getScoreEquipe1() {
        return scoreEquipe1;
    }

    /**
     *
     * @param scoreEquipe1 le score de l'équipe 1
     */
    public void setScoreEquipe1(int scoreEquipe1) {
        this.scoreEquipe1 = scoreEquipe1;
        setChanged();
        notifyObservers();
    }

    /**
     *
     * @return le score de l'équipe 2
     */
    public int getScoreEquipe2() {
        return scoreEquipe2;
    }

    /**
     *
     * @param scoreEquipe2 le score de l'équipe 2
     */
    public void setScoreEquipe2(int scoreEquipe2) {
        this.scoreEquipe2 = scoreEquipe2;
        setChanged();
        notifyObservers();
    }

    /**
     * Ajoute 1 point à l'équipe 1 et notifie ses observeurs
     */
    public void updateScoreEquipe1() {
        this.scoreEquipe1++;
        setChanged();
        notifyObservers();
    }

    /**
     * Ajoute 1 point à l'équipe 2 et notifie ses observeurs
     */
    public void updateScoreEquipe2() {
        this.scoreEquipe2++;
        setChanged();
        notifyObservers();
    }
}
