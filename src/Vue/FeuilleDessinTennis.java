/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vue;

import Controleur.Controleur;
import Controleur.TortueRunnable;
import Modele.PartieDeFoot;
import Modele.PartieDeTennis;
import Modele.Tortue;
import Modele.TortueAmelioree;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 *
 * @author Cédric
 */
public class FeuilleDessinTennis extends JeuDeBalle {
    
    private JLabel scoreJoueur1;
    private JLabel scoreJoueur2;
    private JComboBox listeJoueurs;
    
   public FeuilleDessinTennis(Controleur controleur, FenetrePrincipale fenetre){
       super(controleur,fenetre);
       this.scoreJoueur1 = new JLabel(Integer.toString(0));
       this.scoreJoueur2 = new JLabel(Integer.toString(0));
       this.listeJoueurs = new JComboBox();
       this.listeJoueurs.setFocusable(false);
       Initialisation();
   }
    
   
    private void Initialisation() {

        JLabel titre = new JLabel("Score : ");
        this.getFenetrePrincipale().getScoreBoard().add(titre);
        this.getFenetrePrincipale().getScoreBoard().add(this.scoreJoueur1);
        this.getFenetrePrincipale().getScoreBoard().add(new JLabel(" - "));
        this.getFenetrePrincipale().getScoreBoard().add(this.scoreJoueur2);
        for (Map.Entry<Tortue, TortueRunnable> entry : this.getControleur().getMapTortuesAndThreads().entrySet()) {
            if (entry.getKey() instanceof TortueAmelioree) {
                TortueAmelioree ta = (TortueAmelioree) entry.getKey();
                this.listeJoueurs.addItem(ta);
            }
        }
        this.getFenetrePrincipale().getScoreBoard().add(this.listeJoueurs);
        JButton valider = new JButton("Valider");
        valider.addActionListener(this.getFenetrePrincipale().getActionListener());
        this.getFenetrePrincipale().getScoreBoard().add(valider);
        valider.setFocusable(false);
        /*JButton musique = new JButton("Stop Musique");
        musique.addActionListener(this.getFenetrePrincipale().getActionListener());
        this.getFenetrePrincipale().getScoreBoard().add(musique);
        musique.setFocusable(false);*/
    }
    
     public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawTerrain(g);
    }
     
    public void drawTerrain(Graphics g){
        g.setColor(Color.BLACK);
        g.drawRect(PartieDeTennis.getMarge(), PartieDeTennis.getMarge(), PartieDeTennis.getLongeurTerrain()-(PartieDeTennis.getMarge()*2), PartieDeTennis.getLargeurTerrain()-(PartieDeTennis.getMarge()*2));
        g.drawLine(PartieDeTennis.getLongeurTerrain()/2, PartieDeTennis.getMarge(), PartieDeTennis.getLongeurTerrain()/2, PartieDeTennis.getLargeurTerrain()-PartieDeTennis.getMarge());
        g.drawLine(PartieDeTennis.getMarge(), PartieDeTennis.getMarge()+PartieDeTennis.getLargeurCouloir(), PartieDeTennis.getLongeurTerrain()-PartieDeFoot.getMarge(), PartieDeTennis.getMarge()+PartieDeTennis.getLargeurCouloir());
        g.drawLine(PartieDeTennis.getMarge(), PartieDeTennis.getLargeurTerrain()-PartieDeTennis.getMarge()-PartieDeTennis.getLargeurCouloir(), PartieDeTennis.getLongeurTerrain()-PartieDeFoot.getMarge(), PartieDeTennis.getLargeurTerrain()-PartieDeTennis.getMarge()-PartieDeTennis.getLargeurCouloir());
        g.drawLine(PartieDeTennis.getLongeurTerrain()/2-PartieDeTennis.getLargeurCarreDeService(), PartieDeTennis.getMarge()+PartieDeTennis.getLargeurCouloir(), PartieDeTennis.getLongeurTerrain()/2-PartieDeTennis.getLargeurCarreDeService(), PartieDeTennis.getLargeurTerrain()-PartieDeTennis.getMarge()-PartieDeTennis.getLargeurCouloir());
        g.drawLine(PartieDeTennis.getLongeurTerrain()/2+PartieDeTennis.getLargeurCarreDeService(), PartieDeTennis.getMarge()+PartieDeTennis.getLargeurCouloir(), PartieDeTennis.getLongeurTerrain()/2+PartieDeTennis.getLargeurCarreDeService(), PartieDeTennis.getLargeurTerrain()-PartieDeTennis.getMarge()-PartieDeTennis.getLargeurCouloir());
        g.drawLine(PartieDeTennis.getLongeurTerrain()/2-PartieDeTennis.getLargeurCarreDeService(), PartieDeTennis.getLargeurTerrain()/2, PartieDeTennis.getLongeurTerrain()/2+PartieDeTennis.getLargeurCarreDeService(), PartieDeTennis.getLargeurTerrain()/2);
        g.drawLine(PartieDeTennis.getMarge(), PartieDeTennis.getLargeurTerrain()/2, PartieDeTennis.getMarge()+5, PartieDeTennis.getLargeurTerrain()/2);
        g.drawLine(PartieDeTennis.getLongeurTerrain()-PartieDeTennis.getMarge(), PartieDeTennis.getLargeurTerrain()/2, PartieDeTennis.getLongeurTerrain()-PartieDeTennis.getMarge()-5, PartieDeTennis.getLargeurTerrain()/2);
    } 
}
