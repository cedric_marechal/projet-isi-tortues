package Vue;

import Controleur.Controleur;
import Controleur.TortueRunnable;
import Modele.PartieDeFoot;
import Modele.Tortue;
import Modele.TortueAmelioree;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Map;
import java.util.Observable;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * JPanel Jeu de foot
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class FeuilleDessinFoot extends JeuDeBalle {

    private JLabel but1;
    private JLabel but2;
    private JComboBox listeJoueurs;

    /**
     *
     * @param controleur le controleur principal
     * @param fenetre la fenêtre principale sur laquelle intégrer la feuille
     */
    public FeuilleDessinFoot(Controleur controleur, FenetrePrincipale fenetre) {
        super(controleur, fenetre);
        this.but1 = new JLabel(Integer.toString(0));
        this.but2 = new JLabel(Integer.toString(0));
        this.listeJoueurs = new JComboBox();
        this.listeJoueurs.setFocusable(false);
        this.Initialisation();
    }

    /**
     * Dessine l'objet graphique
     *
     * @param g objet graphique
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawTerrain(g);
    }

    /**
     *
     * @return la combobox contenant la liste des joueurs
     */
    public JComboBox getListeJoueurs() {
        return listeJoueurs;
    }

    /**
     *
     * @param listeJoueurs la combobox contenant la liste des joueurs
     */
    public void setListeJoueurs(JComboBox listeJoueurs) {
        this.listeJoueurs = listeJoueurs;
    }

    /**
     * Dessinne le terrain de foot
     *
     * @param g objet graphique
     */
    public void drawTerrain(Graphics g) {
        g.setColor(Color.WHITE);
        g.drawRect(PartieDeFoot.getMarge(), PartieDeFoot.getMarge(), PartieDeFoot.getLongeurTerrain() - (PartieDeFoot.getMarge() * 2), PartieDeFoot.getLargeurTerrain() - (PartieDeFoot.getMarge() * 2));
        g.drawLine(PartieDeFoot.getLongeurTerrain() / 2, PartieDeFoot.getMarge(), PartieDeFoot.getLongeurTerrain() / 2, PartieDeFoot.getLargeurTerrain() - PartieDeFoot.getMarge());
        //g.drawOval(275, 175, 50, 50);
        int x = (PartieDeFoot.getLongeurTerrain() / 2) - (PartieDeFoot.getRayonMilieuTerrain() / 2);
        int y = (PartieDeFoot.getLargeurTerrain() / 2) - (PartieDeFoot.getRayonMilieuTerrain() / 2);
        g.drawOval(x, y, PartieDeFoot.getRayonMilieuTerrain(), PartieDeFoot.getRayonMilieuTerrain());

        int yCage = (PartieDeFoot.getLargeurTerrain() / 2) - (PartieDeFoot.getLongueurSurfaceReparation() / 2);
        g.drawRect(PartieDeFoot.getMarge(), yCage, PartieDeFoot.getLargeurSurfaceReparation(), PartieDeFoot.getLongueurSurfaceReparation());
        int xCage = PartieDeFoot.getLongeurTerrain() - PartieDeFoot.getLargeurSurfaceReparation() - PartieDeFoot.getMarge();
        g.drawRect(xCage, yCage, PartieDeFoot.getLargeurSurfaceReparation(), PartieDeFoot.getLongueurSurfaceReparation());
        // Cages
        g.drawRect(PartieDeFoot.getMarge() / 2, (PartieDeFoot.getLargeurTerrain() / 2) - (PartieDeFoot.getHauteurCage() / 2), PartieDeFoot.getMarge() / 2, PartieDeFoot.getHauteurCage());
        g.drawRect(PartieDeFoot.getLongeurTerrain() - PartieDeFoot.getMarge(), (PartieDeFoot.getLargeurTerrain() / 2) - (PartieDeFoot.getHauteurCage() / 2), PartieDeFoot.getMarge() / 2, PartieDeFoot.getHauteurCage());
    }

    /**
     *
     * @return le texte du but 1
     */
    public JLabel getBut1() {
        return but1;
    }

    /**
     *
     * @param but1 le texte du but 1
     */
    public void setBut1(JLabel but1) {
        this.but1 = but1;
    }

    /**
     *
     * @return le texte du but 2
     */
    public JLabel getBut2() {
        return but2;
    }

    /**
     *
     * @param but2 le texte du but 2
     */
    public void setBut2(JLabel but2) {
        this.but2 = but2;
    }

    /**
     * Initialise les composants de la fenêtre
     */
    private void Initialisation() {

        JLabel titre = new JLabel("Score : ");
        this.getFenetrePrincipale().getScoreBoard().add(titre);
        this.getFenetrePrincipale().getScoreBoard().add(this.but1);
        this.getFenetrePrincipale().getScoreBoard().add(new JLabel(" - "));
        this.getFenetrePrincipale().getScoreBoard().add(this.but2);
        for (Map.Entry<Tortue, TortueRunnable> entry : this.getControleur().getMapTortuesAndThreads().entrySet()) {
            if (entry.getKey() instanceof TortueAmelioree) {
                TortueAmelioree ta = (TortueAmelioree) entry.getKey();
                this.listeJoueurs.addItem(ta);
            }
        }
        this.getFenetrePrincipale().getScoreBoard().add(this.listeJoueurs);
        JButton valider = new JButton("Valider");
        valider.addActionListener(this.getFenetrePrincipale().getActionListener());
        this.getFenetrePrincipale().getScoreBoard().add(valider);
        valider.setFocusable(false);

        JButton musique = new JButton("Stop Musique");
        musique.addActionListener(this.getFenetrePrincipale().getActionListener());
        this.getFenetrePrincipale().getScoreBoard().add(musique);
        musique.setFocusable(false);
    }

    /**
     * Observe et met à jour les scores
     *
     * @param o la partie de foot observée
     * @param o1
     */
    @Override
    public void update(Observable o, Object o1) {
        super.update(o, o1);
        if (o instanceof PartieDeFoot) {
            PartieDeFoot PF = (PartieDeFoot) o;
            this.but1.setText(Integer.toString(PF.getScoreEquipe1()));
            this.but2.setText(Integer.toString(PF.getScoreEquipe2()));
        }
    }
}
