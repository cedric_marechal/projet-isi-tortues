package Vue;

import Controleur.Controleur;
import Controleur.TortueRunnable;
import Modele.Constante;
import Modele.PartieDeFoot;
import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.util.Map.Entry;

/**
 * JPanel Jeu de balle
 * 
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class JeuDeBalle extends JPanel implements Observer, MouseListener {

    private Controleur controleur;
    private FenetrePrincipale fenetrePrincipale;

    /**
     *
     * @param controleur le controleur principal
     * @param fenetrePrincipale la fenêtre principale sur laquelle intégrer la
     * feuille
     */
    public JeuDeBalle(Controleur controleur, FenetrePrincipale fenetrePrincipale) {
        this.controleur = controleur;
        this.fenetrePrincipale = fenetrePrincipale;
        this.Initialisation();
        this.addMouseListener(this);
    }

    /**
     *
     * @return le controleur principal
     */
    public Controleur getControleur() {
        return controleur;
    }

    /**
     *
     * @param controleur le controleur principal
     */
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    /**
     *
     * @return la fenêtre principale sur laquelle intégrer la feuille
     */
    public FenetrePrincipale getFenetrePrincipale() {
        return fenetrePrincipale;
    }

    /**
     *
     * @param fenetrePrincipale la fenêtre principale sur laquelle intégrer la
     * feuille
     */
    public void setFenetrePrincipale(FenetrePrincipale fenetrePrincipale) {
        this.fenetrePrincipale = fenetrePrincipale;
    }

    /**
     * Initialise la taille de la fenêtre et observe les tortues
     */
    private void Initialisation() {
        this.fenetrePrincipale.getScoreBoard().removeAll();
        this.setBackground(Color.white);
        this.setSize(new Dimension(PartieDeFoot.getLongeurTerrain(), PartieDeFoot.getLargeurTerrain()));
        this.setPreferredSize(new Dimension(PartieDeFoot.getLongeurTerrain(), PartieDeFoot.getLargeurTerrain()));

        // Observation du modèle
        for (Entry<Tortue, TortueRunnable> entry : this.controleur.getMapTortuesAndThreads().entrySet()) {
            entry.getKey().addObserver(this);
        }
        this.controleur.getPartieDeFoot().addObserver(this);
    }

    /**
     * Dessine les objets graphiques, tortues et balles
     *
     * @param g objet graphique
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Color c = g.getColor();

        Dimension dim = getSize();
        if (this.controleur.isJeuDeFoot()) {
            g.setColor(new Color(136, 247, 140));
        } else {
            g.setColor(Color.WHITE);
        }
        g.fillRect(0, 0, dim.width, dim.height);
        g.setColor(c);

        for (Entry<Tortue, TortueRunnable> entry : controleur.getMapTortuesAndThreads().entrySet()) {
            Tortue tortue = entry.getKey();
            if (tortue instanceof TortueAmelioree) {
                this.drawTortueAmelioree(g, (TortueAmelioree) tortue);
            } else if (tortue instanceof TortueBalle) {
                this.drawBalle(g, (TortueBalle) tortue);
            }
        }
    }

    /**
     * Dessinne la balle en forme de cercle
     *
     * @param graph objet graphique
     * @param b la tortue balle
     */
    public void drawBalle(Graphics graph, TortueBalle b) {
        int x = b.getX() - 4;
        int y = b.getY() - 4;
        graph.setColor(b.getCoul());
        graph.fillOval(x, y, 8, 8);
    }

    /**
     * Dessine une tortue améliorée en forme de flèche
     *
     * @param graph objet graphique
     * @param t la tortue améliorée
     */
    public void drawTortueAmelioree(Graphics graph, TortueAmelioree t) {
        if (graph == null) {
            return;
        }

        //Calcule les 3 coins du triangle a partir de
        // la position de la tortue p
        graph.setColor(t.getCoul());

        Point p = new Point(t.getX(), t.getY());
        Polygon arrow = new Polygon();


        //Calcule des deux bases
        //Angle de la droite
        double theta = Constante.ratioDegRad * (-t.getDir());
        //Demi angle au sommet du triangle
        double alpha = Math.atan((float) Constante.rb / (float) Constante.rp);
        //Rayon de la fleche
        double r = Math.sqrt(Constante.rp * Constante.rp + Constante.rb * Constante.rb);
        //Sens de la fleche

        //Pointe
        Point p2 = new Point((int) Math.round(p.x + r * Math.cos(theta)),
                (int) Math.round(p.y - r * Math.sin(theta)));
        arrow.addPoint(p2.x, p2.y);

        //Base 1
        Point p3 = new Point((int) Math.round(p2.x - r * Math.cos(theta + alpha)),
                (int) Math.round(p2.y + r * Math.sin(theta + alpha)));

        arrow.addPoint(p3.x, p3.y);
        //Base 2
        Point p4 = new Point((int) Math.round(p2.x - r * Math.cos(theta - alpha)),
                (int) Math.round(p2.y + r * Math.sin(theta - alpha)));

        arrow.addPoint(p4.x, p4.y);

        arrow.addPoint(p2.x, p2.y);

        graph.drawString(t.getNom(), p2.x, p2.y + 1);

        t.setArrow(arrow);
        graph.fillPolygon(arrow);

    }

    /**
     * Observe les tortues, au moindre changement, redessine tout
     *
     * @param o tortue observée
     * @param o1
     */
    @Override
    public void update(Observable o, Object o1) {
        this.repaint();
    }

    /**
     * Ouvre la fenêtre des caractéristiques de la tortue au clic sur une tortue
     *
     * @param me clic de souris
     */
    @Override
    public void mouseClicked(MouseEvent me) {
        for (Entry<Tortue, TortueRunnable> entry : controleur.getMapTortuesAndThreads().entrySet()) {
            Tortue tortue = entry.getKey();
            if (tortue instanceof TortueAmelioree) {
                TortueAmelioree tortueAmelioree = (TortueAmelioree) tortue;
                if (tortueAmelioree.getArrow().contains(me.getX(), me.getY())) {
                    this.controleur.setTortueCourante(tortueAmelioree);
                    CaracteristiqueTortue fenetreTortue = new CaracteristiqueTortue(this.controleur, tortueAmelioree);
                    fenetreTortue.setVisible(true);
                }
            }
        }
    }

    /**
     *
     * @param me
     */
    @Override
    public void mousePressed(MouseEvent me) {}

    /**
     *
     * @param me
     */
    @Override
    public void mouseReleased(MouseEvent me) {}

    /**
     *
     * @param me
     */
    @Override
    public void mouseEntered(MouseEvent me) {}

    /**
     *
     * @param me
     */
    @Override
    public void mouseExited(MouseEvent me) {}
}
