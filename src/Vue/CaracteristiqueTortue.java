package Vue;

import Controleur.CaracteristiqueVueListener;
import Controleur.Controleur;
import Modele.Strategie.Cooperation.Collectif;
import Modele.Strategie.Cooperation.Personnel;
import Modele.Strategie.Cooperation.TirJoueur;
import Modele.Strategie.Position.Attaquant;
import Modele.Strategie.Position.Defenseur;
import Modele.Strategie.Position.Gardien;
import Modele.Strategie.Position.Joueur1;
import Modele.Strategie.Position.Joueur2;
import Modele.Strategie.Position.Milieu;
import Modele.TortueAmelioree;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JTextField;

/**
 * Fenêtre des caractéristiques de la tortue
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class CaracteristiqueTortue extends JFrame {

    /**
     * Creates new form CaracteristiqueTortue
     */
    private Controleur controleur;
    private CaracteristiqueVueListener vueListener;
    private TortueAmelioree tortue;

    /**
     *
     * @param controleur le controleur de l'application
     * @param tortue la tortue pour laquelle les caractéristiques vont être
     * modifiées
     */
    public CaracteristiqueTortue(Controleur controleur, TortueAmelioree tortue) {
        this.controleur = controleur;
        this.vueListener = new CaracteristiqueVueListener(controleur, this);
        this.tortue = tortue;
        initComponents();
        this.sliderVitesse.setValue(tortue.getVitesse());
        initialiserComboboxesStrategies();
        this.boutonAppliquer.addActionListener(this.vueListener);
        this.boutonValider.addActionListener(this.vueListener);
        this.nom.setText(tortue.getNom());
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    /**
     * Initialise la combobox avec les stratégies de jeu
     */
    public final void initialiserComboboxesStrategies() {
        this.listeStrategiePosition.removeAllItems();
        this.listeStrategiePosition.addItem(this.controleur.creerStrategieAttaquant());
        this.listeStrategiePosition.addItem(this.controleur.creerStrategieDefenseur());
        this.listeStrategiePosition.addItem(this.controleur.creerStrategieGardien());
        this.listeStrategiePosition.addItem(this.controleur.creerStrategieMilieu());
        this.listeStrategiePosition.addItem(this.controleur.creerStrategieJoueur1());
        this.listeStrategiePosition.addItem(this.controleur.creerStrategieJoueur2());
        if (tortue.getStrategie().getPosition() instanceof Attaquant) {
            this.listeStrategiePosition.setSelectedIndex(0);
        } else if (tortue.getStrategie().getPosition() instanceof Defenseur) {
            this.listeStrategiePosition.setSelectedIndex(1);
        } else if (tortue.getStrategie().getPosition() instanceof Gardien) {
            this.listeStrategiePosition.setSelectedIndex(2);
        } else if (tortue.getStrategie().getPosition() instanceof Milieu) {
            this.listeStrategiePosition.setSelectedIndex(3);
        } else if (tortue.getStrategie().getPosition() instanceof Joueur1) {
            this.listeStrategiePosition.setSelectedIndex(4);
        } else if (tortue.getStrategie().getPosition() instanceof Joueur2) {
            this.listeStrategiePosition.setSelectedIndex(5);
        }

        this.listeStrategieCooperation.removeAllItems();
        this.listeStrategieCooperation.addItem(this.controleur.creerStrategieCollectif());
        this.listeStrategieCooperation.addItem(this.controleur.creerStrategiePersonnel());
        this.listeStrategieCooperation.addItem(this.controleur.creerStrategieTirJoueur());
        if (tortue.getStrategie().getCooperation() instanceof Collectif) {
            this.listeStrategieCooperation.setSelectedIndex(0);
        } else if (tortue.getStrategie().getCooperation() instanceof Personnel) {
            this.listeStrategieCooperation.setSelectedIndex(1);
        } else if (tortue.getStrategie().getCooperation() instanceof TirJoueur) {
            this.listeStrategieCooperation.setSelectedIndex(2);
        }
    }

    /**
     *
     * @return la combobox des stratégies de coopération
     */
    public JComboBox getListeStrategieCooperation() {
        return listeStrategieCooperation;
    }

    /**
     *
     * @param listeStrategieCooperation la combobox des stratégies de
     * coopération
     */
    public void setListeStrategieCooperation(JComboBox listeStrategieCooperation) {
        this.listeStrategieCooperation = listeStrategieCooperation;
    }

    /**
     *
     * @return la combobox des stratégies de positionnement
     */
    public JComboBox getListeStrategiePosition() {
        return listeStrategiePosition;
    }

    /**
     *
     * @param listeStrategiePosition la combobox des stratégies de
     * positionnement
     */
    public void setListeStrategiePosition(JComboBox listeStrategiePosition) {
        this.listeStrategiePosition = listeStrategiePosition;
    }

    /**
     *
     * @return le champ nom
     */
    public JTextField getNom() {
        return nom;
    }

    /**
     *
     * @param nom le champ nom
     */
    public void setNom(JTextField nom) {
        this.nom = nom;
    }

    /**
     *
     * @return le slider de vitesse
     */
    public JSlider getSliderVitesse() {
        return sliderVitesse;
    }

    /**
     *
     * @param sliderVitesse le slider de vitesse
     */
    public void setSliderVitesse(JSlider sliderVitesse) {
        this.sliderVitesse = sliderVitesse;
    }

    /**
     *
     * @return le listener de cette fenêtre
     */
    public CaracteristiqueVueListener getVueListener() {
        return vueListener;
    }

    /**
     *
     * @param vueListener le listener de cette fenêtre
     */
    public void setVueListener(CaracteristiqueVueListener vueListener) {
        this.vueListener = vueListener;
    }

    /**
     *
     * @return la tortue pour laquelle les caractéristiques vont être modifiées
     */
    public TortueAmelioree getTortue() {
        return tortue;
    }

    /**
     *
     * @param tortue la tortue pour laquelle les caractéristiques vont être
     * modifiées
     */
    public void setTortue(TortueAmelioree tortue) {
        this.tortue = tortue;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        sliderVitesse = new javax.swing.JSlider();
        nom = new javax.swing.JTextField();
        boutonAppliquer = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        listeStrategiePosition = new javax.swing.JComboBox();
        listeStrategieCooperation = new javax.swing.JComboBox();
        boutonValider = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Caracteristiques de la tortue");

        jLabel2.setText("Nom");

        jLabel3.setText("Strategie Cooperation");

        jLabel4.setText("Vitesse");

        sliderVitesse.setMaximum(150);

        boutonAppliquer.setText("Appliquer");

        jLabel5.setText("Strategie Position");

        listeStrategiePosition.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        listeStrategieCooperation.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        boutonValider.setText("Valider");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(boutonAppliquer)
                        .addGap(18, 18, 18)
                        .addComponent(boutonValider))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(sliderVitesse, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(nom)
                            .addComponent(listeStrategiePosition, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(listeStrategieCooperation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(134, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel1)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(listeStrategieCooperation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(listeStrategiePosition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(sliderVitesse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boutonAppliquer)
                    .addComponent(boutonValider))
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boutonAppliquer;
    private javax.swing.JButton boutonValider;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JComboBox listeStrategieCooperation;
    private javax.swing.JComboBox listeStrategiePosition;
    private javax.swing.JTextField nom;
    private javax.swing.JSlider sliderVitesse;
    // End of variables declaration//GEN-END:variables
}
