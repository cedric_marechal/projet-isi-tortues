package Vue;

import Controleur.ClavierListener;
import Controleur.Controleur;
import Controleur.VueListener;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Fenêtre principale
 * 
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class FenetrePrincipale extends JFrame {

    public static final Dimension VGAP = new Dimension(1, 5);
    public static final Dimension HGAP = new Dimension(5, 1);
    private JeuDeBalle feuille;
    // private JTextField inputValue;
    private Controleur controleur;
    private VueListener actionListener;
    private ClavierListener clavier;
    private JPanel content;
    private JPanel scoreBoard;

    /**
     * Initialise la fenêtre principale
     *
     * @param controleur le contrôleur de l'application
     */
    public FenetrePrincipale(Controleur controleur) {
        super("Les Tortues au stade");
        this.setFocusable(true);
        this.controleur = controleur;
        this.content = new JPanel();
        this.scoreBoard = new JPanel();
        this.actionListener = new VueListener(controleur, this);
        this.clavier = new ClavierListener(controleur);
        this.addKeyListener(clavier);
        logoInit();

        this.setLocationRelativeTo(null);
    }

    /**
     *
     * @return l'ActionListener sur cette fenêtre
     */
    public VueListener getActionListener() {
        return actionListener;
    }

    /**
     *
     * @param actionListener l'ActionListener sur cette fenêtre
     */
    public void setActionListener(VueListener actionListener) {
        this.actionListener = actionListener;
    }

    /**
     *
     * @return le contenu du panel
     */
    public JPanel getContent() {
        return content;
    }

    /**
     *
     * @param content le contenu du panel
     */
    public void setContent(JPanel content) {
        this.content = content;
    }

    /**
     *
     * @return le panel des scores
     */
    public JPanel getScoreBoard() {
        return scoreBoard;
    }

    /**
     *
     * @param control le panel des scores
     */
    public void setScoreBoard(JPanel control) {
        this.scoreBoard = control;
    }

    /**
     *
     * @return la feuille du jeu de balle
     */
    public JeuDeBalle getFeuille() {
        return feuille;
    }

    /**
     * Ajoute la feuille et recadre la fenêtre
     *
     * @param feuille la feuille du jeu de balle
     */
    public void setFeuille(JeuDeBalle feuille) {
        this.feuille = feuille;
        this.content.removeAll();
        this.content.add(this.feuille);
        this.pack();
        this.setLocationRelativeTo(null);
    }

    /**
     * Initialise la fenêtre et ses composants
     */
    private void logoInit() {
        getContentPane().setLayout(new BorderLayout(10, 10));

        // Boutons
        JToolBar toolBar = new JToolBar();
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(toolBar);

        getContentPane().add(buttonPanel, "North");

        addButton(toolBar, "Reset", "Nouveau dessin", "/icons/New24.gif");

        toolBar.add(Box.createRigidArea(HGAP));
        addButton(toolBar, "+Joueur", "+Joueur", null);
        addButton(toolBar, "+Balle", "+Balle", null);
        addButton(toolBar, "Start", "Start", null);
        addButton(toolBar, "Pause", "Pause", null);
        addButton(toolBar, "Reprise", "Reprise", null);


        String[] colorStrings = {"noir", "bleu", "cyan", "gris fonce", "rouge",
            "vert", "gris clair", "magenta", "orange",
            "gris", "rose", "jaune"};

        // Create the combo box
        toolBar.add(Box.createRigidArea(HGAP));
        JLabel colorLabel = new JLabel("   Couleur: ");
        toolBar.add(colorLabel);
        JComboBox colorList = new JComboBox(colorStrings);
        toolBar.add(colorList);
        colorList.setFocusable(false);

        // Menus
        JMenuBar menubar = new JMenuBar();
        setJMenuBar(menubar);	// on installe le menu bar
        JMenu menuFile = new JMenu("File"); // on installe le premier menu
        menubar.add(menuFile);

        addMenuItem(menuFile, "Reset", "Reset", KeyEvent.VK_N);
        addMenuItem(menuFile, "Quitter", "Quitter", KeyEvent.VK_Q);

        JMenu menuCommandes = new JMenu("Commandes"); // on installe le premier menu
        menubar.add(menuCommandes);

        JMenu menuHelp = new JMenu("Aide"); // on installe le premier menu
        menubar.add(menuHelp);
        addMenuItem(menuHelp, "Aide", "Help", -1);
        addMenuItem(menuHelp, "A propos", "About", -1);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // les boutons du bas
        JPanel p2 = new JPanel(new GridLayout());
        JButton btJeuDeBalle = new JButton("StartJeuDeBalle");
        btJeuDeBalle.setFocusable(false);
        btJeuDeBalle.addActionListener(this.actionListener);
        JButton btJeuDeFoot = new JButton("StartJeuDeFoot");
        btJeuDeFoot.setFocusable(false);
        btJeuDeFoot.addActionListener(this.actionListener);
        JButton btJeuDeTennis = new JButton("StartJeuDeTennis");
        btJeuDeTennis.setFocusable(false);
        btJeuDeTennis.addActionListener(this.actionListener);
        p2.add(btJeuDeBalle);
        p2.add(btJeuDeFoot);
        p2.add(btJeuDeTennis);
        JPanel droite = new JPanel(new BorderLayout());
        droite.add(p2, BorderLayout.PAGE_START);
        droite.add(this.scoreBoard, BorderLayout.LINE_START);
        getContentPane().add(droite, "East");
        getContentPane().add(content, "West");
        pack();
        setVisible(true);
    }

    /**
     * Utilitaires pour installer des boutons
     *
     * @param p le composant
     * @param name le nom du bouton
     * @param tooltiptext le nom de la tooltip
     * @param imageName le nom de l'image
     */
    public void addButton(JComponent p, String name, String tooltiptext, String imageName) {
        JButton b;
        if ((imageName == null) || (imageName.equals(""))) {
            b = (JButton) p.add(new JButton(name));
        } else {
            java.net.URL u = this.getClass().getResource(imageName);
            if (u != null) {
                ImageIcon im = new ImageIcon(u);
                b = (JButton) p.add(new JButton(im));
            } else {
                b = (JButton) p.add(new JButton(name));
            }
            b.setActionCommand(name);
        }

        b.setToolTipText(tooltiptext);
        b.setBorder(BorderFactory.createRaisedBevelBorder());
        b.setMargin(new Insets(0, 0, 0, 0));
        b.addActionListener(this.actionListener);
        b.setFocusable(false);
    }

    /**
     * Ajoute un label dans le menu
     *
     * @param m le conteneur menu
     * @param label le nom du label
     * @param command le nom de la commande
     * @param key la clé
     */
    public void addMenuItem(JMenu m, String label, String command, int key) {
        JMenuItem menuItem;
        menuItem = new JMenuItem(label);
        m.add(menuItem);

        menuItem.setActionCommand(command);
        menuItem.addActionListener(this.actionListener);
        if (key > 0) {
            if (key != KeyEvent.VK_DELETE) {
                menuItem.setAccelerator(KeyStroke.getKeyStroke(key, Event.CTRL_MASK, false));
            } else {
                menuItem.setAccelerator(KeyStroke.getKeyStroke(key, 0, false));
            }
        }
    }

    /**
     * Charge le jeu de balle
     */
    public void loadJeuDeBalle() {
        this.setFeuille(new JeuDeBalle(this.controleur, this));
    }

    /**
     * Charge le jeu de foot
     */
    public void loadJeuDeFoot() {
        this.setFeuille(new FeuilleDessinFoot(this.controleur, this));
    }
    
    public void loadJeuDeTennis(){
        this.setFeuille(new FeuilleDessinTennis(this.controleur, this));
    }
}
