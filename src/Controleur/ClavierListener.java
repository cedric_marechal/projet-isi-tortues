package Controleur;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Un listener sur le clavier
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class ClavierListener implements KeyListener {

    private Controleur controleur;

    /**
     *
     * @param controleur le controleur de l'application
     */
    public ClavierListener(Controleur controleur) {
        this.controleur = controleur;
    }

    /**
     *
     * @param e l'évènement généré par le clavier
     */
    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Ajoute dans les Threads des tortues controllables l'évènement e s'il
     * correspond à une touche valide de déplacement
     *
     * @param e l'évènement généré par le clavier
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT
                || e.getKeyCode() == KeyEvent.VK_Z || e.getKeyCode() == KeyEvent.VK_S || e.getKeyCode() == KeyEvent.VK_Q || e.getKeyCode() == KeyEvent.VK_D) {
            for (TortueRunnable tortueRun : this.controleur.getListeTortuesControllables()) {
                tortueRun.ajouterToucheClavier(e);
            }
        }
    }

    /**
     * Supprime dans les Threads des tortues controllables l'évènement e s'il
     * correspond à une touche valide de déplacement
     *
     * @param e l'évènement généré par le clavier
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT
                || e.getKeyCode() == KeyEvent.VK_Z || e.getKeyCode() == KeyEvent.VK_S || e.getKeyCode() == KeyEvent.VK_Q || e.getKeyCode() == KeyEvent.VK_D) {
            for (TortueRunnable tortueRun : this.controleur.getListeTortuesControllables()) {
                tortueRun.supprimerToucheClavier(e);
            }
        }
    }
}
