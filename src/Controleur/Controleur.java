package Controleur;

import Modele.Constante;
import Modele.PartieDeFoot;
import Modele.PartieDeTennis;
import Modele.Strategie.Cooperation.Collectif;
import Modele.Strategie.Cooperation.Cooperation;
import Modele.Strategie.Cooperation.Personnel;
import Modele.Strategie.Cooperation.StrategieJeuDeBalle;
import Modele.Strategie.Cooperation.TirJoueur;
import Modele.Strategie.Position.Aleatoire;
import Modele.Strategie.Position.Attaquant;
import Modele.Strategie.Position.Defenseur;
import Modele.Strategie.Position.Gardien;
import Modele.Strategie.Position.Joueur1;
import Modele.Strategie.Position.Joueur2;
import Modele.Strategie.Position.Milieu;
import Modele.Strategie.Position.Position;
import Modele.Strategie.Strategie;
import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;
import Vue.JeuDeBalle;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Le contrôleur de toute l'application
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Controleur {

    private Musique musique = new Musique("ressources/stade.wav", true);
    private Map<Tortue, TortueRunnable> mapTortuesAndThreads;
    private ArrayList<TortueRunnable> listeTortuesControllables;
    private Tortue tortueCourante;
    private Point but1;
    private Point but2;
    private PartieDeFoot partieDeFoot;
    private boolean jeuDeFoot;
    private boolean jeuLance, jeuEnCours;

    /**
     * Définit les zones de but et initialise les listes
     */
    public Controleur() {
        this.but1 = new Point(PartieDeFoot.getMarge() / 2, (PartieDeFoot.getLargeurTerrain() / 2) - (PartieDeFoot.getHauteurCage() / 2));
        this.but2 = new Point(PartieDeFoot.getLongeurTerrain() - PartieDeFoot.getMarge(), (PartieDeFoot.getLargeurTerrain() / 2) - (PartieDeFoot.getHauteurCage() / 2));
        this.partieDeFoot = new PartieDeFoot();
        this.mapTortuesAndThreads = new HashMap();
        this.listeTortuesControllables = new ArrayList<TortueRunnable>();
        this.jeuLance = false;
        this.jeuEnCours = false;
    }

    /**
     *
     * @return le type de jeu
     */
    public boolean isJeuDeFoot() {
        return jeuDeFoot;
    }

    /**
     *
     * @param jeuDeFoot le type de jeu
     */
    public void setJeuDeFoot(boolean jeuDeFoot) {
        this.jeuDeFoot = jeuDeFoot;
    }

    /**
     *
     * @return la musique de fond
     */
    public Musique getMusique() {
        return musique;
    }

    /**
     *
     * @param musique la musique de fond
     */
    public void setMusique(Musique musique) {
        this.musique = musique;
    }

    /**
     *
     * @return la position (coin gauche) du but 1
     */
    public Point getBut1() {
        return but1;
    }

    /**
     *
     * @param but1 la position (coin gauche) du but 1
     */
    public void setBut1(Point but1) {
        this.but1 = but1;
    }

    /**
     *
     * @return la position (coin gauche) du but 2
     */
    public Point getBut2() {
        return but2;
    }

    /**
     *
     * @param but2 la position (coin gauche) du but 2
     */
    public void setBut2(Point but2) {
        this.but2 = but2;
    }

    /**
     *
     * @return la partie de foot
     */
    public PartieDeFoot getPartieDeFoot() {
        return partieDeFoot;
    }

    /**
     *
     * @param partieDeFoot la partie de foot
     */
    public void setPartieDeFoot(PartieDeFoot partieDeFoot) {
        this.partieDeFoot = partieDeFoot;
    }

    /**
     *
     * @return la liste des Threads des tortues controllables
     */
    public ArrayList<TortueRunnable> getListeTortuesControllables() {
        return listeTortuesControllables;
    }

    /**
     *
     * @param listeTortuesControllables la liste des Threads des tortues
     * controllables
     */
    public void setListeTortuesControllables(ArrayList<TortueRunnable> listeTortuesControllables) {
        this.listeTortuesControllables = listeTortuesControllables;
    }

    /**
     * Ajoute 1 point à l'équipe 1
     */
    public void updateScoreEquipe1() {
        this.partieDeFoot.updateScoreEquipe1();
    }

    /**
     * Ajoute 1 point à l'équipe 2
     */
    public void updateScoreEquipe2() {
        this.partieDeFoot.updateScoreEquipe2();
    }

    /**
     * A utiliser lorsqu'il y a un but
     * <ul>
     * <li>Met en pause les Threads des joueurs</li>
     * <li>Replace les joueurs à leur position initiale</li>
     * <li>Attend 3s</li>
     * <li>Redémarre les Thread</li>
     * </ul>
     */
    public void reInitialiserTortues() {
        pauseAll();
        for (Entry<Tortue, TortueRunnable> entry : this.mapTortuesAndThreads.entrySet()) {
            Tortue tortue = entry.getKey();
            tortue.setPosition(tortue.getxInitial(), tortue.getyInitial());
            if (tortue instanceof TortueBalle) {
                tortue.setVitesse(0);
            }
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
        restartAll();
    }

    /**
     * Initialise un jeu de balle avec les paramètres par défaut
     */
    public void initialiserTortuesJeuDeBalle() {
        this.mapTortuesAndThreads.clear();
        TortueAmelioree tortue1 = new TortueAmelioree(300, 50, 90, 50, 100, 500, 1, Color.BLACK, "Cedric", new Strategie(new Aleatoire(), new StrategieJeuDeBalle()));
        TortueAmelioree tortue2 = new TortueAmelioree(300, 100, 180, 70, 100, 700, 1, Color.BLUE, "Evgeniy", new Strategie(new Aleatoire(), new StrategieJeuDeBalle()));
        this.tortueCourante = tortue1;
        TortueRunnable tr1 = new TortueRunnable(tortue1, this);
        TortueRunnable tr2 = new TortueRunnable(tortue2, this);
        TortueBalle balle = new TortueBalle(300, 200, 0, 0, Color.RED);
        TortueRunnable trBalle = new TortueRunnable(balle, this);
        this.mapTortuesAndThreads.put(tortue1, tr1);
        this.mapTortuesAndThreads.put(tortue2, tr2);
        this.mapTortuesAndThreads.put(balle, trBalle);
        ajouterTortuesConnues();
        
        this.jeuLance = false;
        this.jeuEnCours = false;
    }

    public void initialiserTortuesJeuDeTennis(){
        this.mapTortuesAndThreads.clear();
        TortueAmelioree joueur1 = new TortueAmelioree(PartieDeTennis.getMarge()-5,PartieDeTennis.getLargeurTerrain()/2+10,90,30,150,100,1,Color.BLUE,"Federer", new Strategie(new Gardien(),new Collectif()));
        TortueRunnable trJoueur1 = new TortueRunnable(joueur1,this);
        TortueAmelioree joueur2 = new TortueAmelioree(PartieDeTennis.getLongeurTerrain()-PartieDeTennis.getMarge()+5,PartieDeTennis.getLargeurTerrain()/2-10,90,30,150,100,2,Color.GREEN,"Djokovic", new Strategie(new Gardien(),new Collectif()));
        TortueRunnable trJoueur2 = new TortueRunnable(joueur2,this);
        this.mapTortuesAndThreads.put(joueur1, trJoueur1);
        this.mapTortuesAndThreads.put(joueur2, trJoueur2);
        TortueBalle balle = new TortueBalle(PartieDeTennis.getMarge(), PartieDeTennis.getLargeurTerrain()/2+10, 0, 0, Color.RED);
        TortueRunnable trBalle = new TortueRunnable(balle, this);
        this.mapTortuesAndThreads.put(balle, trBalle);
        ajouterTortuesConnues();
        this.jeuLance = false;
        this.jeuEnCours = false;
    }
    /**
     * Initialise un jeu de Foot avec les paramètres par défaut
     */
    public void initialiserTortuesJeuDeFoot() {
        this.mapTortuesAndThreads.clear();
        int positionXDepart = PartieDeFoot.getMarge() + PartieDeFoot.getLargeurSurfaceReparation();
        int positionX = ((PartieDeFoot.getLongeurTerrain() / 2) - PartieDeFoot.getLargeurSurfaceReparation() - PartieDeFoot.getMarge()) / 4;
        int positionY = (PartieDeFoot.getLargeurTerrain() - (PartieDeFoot.getMarge() * 2)) / 5;

        TortueAmelioree gardien = new TortueAmelioree(PartieDeFoot.getMarge() + PartieDeFoot.getLargeurSurfaceReparation() / 2, PartieDeFoot.getLargeurTerrain() / 2, 90, 60, 150, 100, 1, Color.BLUE, "Gardien", new Strategie(new Gardien(), new Collectif()));
        this.tortueCourante = gardien;
        TortueRunnable trGardien = new TortueRunnable(gardien, this);

        TortueAmelioree tortue1 = new TortueAmelioree(positionXDepart + positionX, (int) (positionY * 1.5), 180, 30, 150, 100, 1, Color.BLUE, "IA1", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr1 = new TortueRunnable(tortue1, this);
        TortueAmelioree tortue2 = new TortueAmelioree(positionXDepart + positionX, (int) (positionY * 2.5), 0, 30, 150, 100, 1, Color.BLUE, "IA2", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr2 = new TortueRunnable(tortue2, this);
        TortueAmelioree tortue3 = new TortueAmelioree(positionXDepart + positionX, (int) (positionY * 3.5), 75, 30, 130, 100, 1, Color.BLUE, "IA3", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr3 = new TortueRunnable(tortue3, this);

        TortueAmelioree tortue4 = new TortueAmelioree(positionXDepart + (positionX * 2), positionY, 75, 40, 130, 100, 1, Color.BLUE, "IA4", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr4 = new TortueRunnable(tortue4, this);
        TortueAmelioree tortue5 = new TortueAmelioree(positionXDepart + (positionX * 2), positionY * 2, 180, 60, 150, 100, 1, Color.BLUE, "IA5", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr5 = new TortueRunnable(tortue5, this);
        TortueAmelioree tortue6 = new TortueAmelioree(positionXDepart + (positionX * 2), positionY * 3, 0, 50, 150, 100, 1, Color.BLUE, "IA6", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr6 = new TortueRunnable(tortue6, this);
        TortueAmelioree tortue7 = new TortueAmelioree(positionXDepart + (positionX * 2), positionY * 4, 75, 40, 130, 100, 1, Color.BLUE, "IA7", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr7 = new TortueRunnable(tortue7, this);

        TortueAmelioree tortue8 = new TortueAmelioree(positionXDepart + (positionX * 3), (int) (positionY * 1.5), 75, 40, 130, 100, 1, Color.BLUE, "IA8", new Strategie(new Attaquant(), new Collectif()));
        TortueRunnable tr8 = new TortueRunnable(tortue8, this);
        TortueAmelioree tortue9 = new TortueAmelioree(positionXDepart + (positionX * 3), (int) (positionY * 2.5), 75, 40, 130, 100, 1, Color.BLUE, "IA9", new Strategie(new Attaquant(), new Collectif()));
        TortueRunnable tr9 = new TortueRunnable(tortue9, this);
        TortueAmelioree tortue10 = new TortueAmelioree(positionXDepart + (positionX * 3), (int) (positionY * 3.5), 75, 100, 130, 100, 1, new Color(5, 188, 226), "J2", new Strategie(new Joueur2(), new TirJoueur()));
        TortueRunnable tr10 = new TortueRunnable(tortue10, this);
        this.listeTortuesControllables.add(tr10);



        TortueAmelioree gardien2 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - (PartieDeFoot.getMarge() + PartieDeFoot.getLargeurSurfaceReparation() / 2), PartieDeFoot.getLargeurTerrain() / 2, 90, 50, 150, 100, 2, Color.MAGENTA, "Gardien", new Strategie(new Gardien(), new Collectif()));
        this.tortueCourante = gardien;
        TortueRunnable trGardien2 = new TortueRunnable(gardien2, this);

        TortueAmelioree tortue11 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - positionX, positionY, 180, 30, 150, 100, 2, Color.MAGENTA, "IA10", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr11 = new TortueRunnable(tortue11, this);
        TortueAmelioree tortue12 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - positionX, positionY * 2, 0, 30, 150, 100, 2, Color.MAGENTA, "IA11", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr12 = new TortueRunnable(tortue12, this);
        TortueAmelioree tortue13 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - positionX, positionY * 3, 75, 30, 130, 100, 2, Color.MAGENTA, "IA12", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr13 = new TortueRunnable(tortue13, this);
        TortueAmelioree tortue14 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - positionX, positionY * 4, 75, 30, 130, 100, 2, Color.MAGENTA, "IA13", new Strategie(new Defenseur(), new Collectif()));
        TortueRunnable tr14 = new TortueRunnable(tortue14, this);

        TortueAmelioree tortue15 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - (positionX * 2), positionY, 75, 40, 130, 100, 2, Color.MAGENTA, "IA14", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr15 = new TortueRunnable(tortue15, this);
        TortueAmelioree tortue16 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - (positionX * 2), positionY * 2, 0, 50, 150, 100, 2, Color.MAGENTA, "IA15", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr16 = new TortueRunnable(tortue16, this);
        TortueAmelioree tortue17 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - (positionX * 2), positionY * 3, 75, 40, 130, 100, 2, Color.MAGENTA, "IA16", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr17 = new TortueRunnable(tortue17, this);
        TortueAmelioree tortue18 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - (positionX * 2), positionY * 4, 75, 40, 130, 100, 2, Color.MAGENTA, "IA17", new Strategie(new Milieu(), new Collectif()));
        TortueRunnable tr18 = new TortueRunnable(tortue18, this);

        TortueAmelioree tortue19 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - (positionX * 3), positionY * 2, 75, 40, 130, 100, 2, Color.MAGENTA, "IA18", new Strategie(new Attaquant(), new Collectif()));
        TortueRunnable tr19 = new TortueRunnable(tortue19, this);
        TortueAmelioree tortue20 = new TortueAmelioree(PartieDeFoot.getLongeurTerrain() - positionXDepart - (positionX * 3), positionY * 3, 75, 100, 130, 100, 2, Color.PINK, "J1", new Strategie(new Joueur1(), new TirJoueur()));
        TortueRunnable tr20 = new TortueRunnable(tortue20, this);
        this.listeTortuesControllables.add(tr20);

        TortueBalle balle = new TortueBalle(PartieDeFoot.getLongeurTerrain() / 2, PartieDeFoot.getLargeurTerrain() / 2, 0, 0, Color.RED);
        TortueRunnable trBalle = new TortueRunnable(balle, this);

        //TortueBalle balle2 = new TortueBalle(PartieDeFoot.getLongeurTerrain()/2+50,PartieDeFoot.getLargeurTerrain()/2,0,0,Color.RED);
        //TortueRunnable trBalle2 = new TortueRunnable(balle2,this);
        this.mapTortuesAndThreads.put(gardien, trGardien);
        this.mapTortuesAndThreads.put(tortue1, tr1);
        this.mapTortuesAndThreads.put(tortue2, tr2);
        this.mapTortuesAndThreads.put(tortue3, tr3);
        this.mapTortuesAndThreads.put(tortue4, tr4);
        this.mapTortuesAndThreads.put(tortue5, tr5);
        this.mapTortuesAndThreads.put(tortue6, tr6);
        this.mapTortuesAndThreads.put(tortue7, tr7);
        this.mapTortuesAndThreads.put(tortue8, tr8);
        this.mapTortuesAndThreads.put(tortue9, tr9);
        this.mapTortuesAndThreads.put(tortue10, tr10);

        this.mapTortuesAndThreads.put(gardien2, trGardien2);
        this.mapTortuesAndThreads.put(tortue11, tr11);
        this.mapTortuesAndThreads.put(tortue12, tr12);
        this.mapTortuesAndThreads.put(tortue13, tr13);
        this.mapTortuesAndThreads.put(tortue14, tr14);
        this.mapTortuesAndThreads.put(tortue15, tr15);
        this.mapTortuesAndThreads.put(tortue16, tr16);
        this.mapTortuesAndThreads.put(tortue17, tr17);
        this.mapTortuesAndThreads.put(tortue18, tr18);
        this.mapTortuesAndThreads.put(tortue19, tr19);
        this.mapTortuesAndThreads.put(tortue20, tr20);

        this.mapTortuesAndThreads.put(balle, trBalle);
        //this.mapTortuesAndThreads.put(balle2, trBalle2);
        ajouterTortuesConnues();
        
        this.jeuLance = false;
        this.jeuEnCours = false;
    }

    /**
     * Ajoute à chaque tortue de la liste mapTortuesAndThreads la liste des
     * tortues connues
     */
    public void ajouterTortuesConnues() {
        for (Entry<Tortue, TortueRunnable> entry : this.mapTortuesAndThreads.entrySet()) {
            if (entry.getKey() instanceof TortueAmelioree) {
                TortueAmelioree ta = (TortueAmelioree) entry.getKey();
                for (Entry<Tortue, TortueRunnable> entryTortue : this.mapTortuesAndThreads.entrySet()) {
                    if (entryTortue.getKey() != ta) {
                        ta.ajouterTortueConnue(entryTortue.getKey());
                    }
                }
            }
        }
    }

    /**
     *
     * @return la liste des tortues et leur Thread
     */
    public Map<Tortue, TortueRunnable> getMapTortuesAndThreads() {
        return mapTortuesAndThreads;
    }

    /**
     *
     * @param mapTortuesAndThreads la liste des tortues et leur Thread
     */
    public void setMapTortuesAndThreads(Map<Tortue, TortueRunnable> mapTortuesAndThreads) {
        this.mapTortuesAndThreads = mapTortuesAndThreads;
    }

    /**
     *
     * @return la tortue courante
     */
    public Tortue getTortueCourante() {
        return tortueCourante;
    }

    /**
     *
     * @param tortueCourante la tortue courante
     */
    public void setTortueCourante(Tortue tortueCourante) {
        this.tortueCourante = tortueCourante;
    }

    /**
     * Ajoute la tortue amélioree dans la liste de tortue, lui crée un Thread
     * puis ajoute le jeu de balle en tant qu'obsverveur
     *
     * @param jdb la fenetre jeu de balle
     */
    public void addTortueAmelioree(JeuDeBalle jdb) {
        Tortue t = new TortueAmelioree();
        TortueRunnable tr = new TortueRunnable(t, this);
        mapTortuesAndThreads.put(t, tr);
        t.addObserver(jdb);
        ajouterTortuesConnues();
        if (this.jeuLance) {
            tr.getThread().start();
            tr.setFin(false);
            if (!this.jeuEnCours) {
                tr.pause();
            }
        }
    }

    /**
     * Ajoute la tortue balle dans la liste de tortue, lui crée un Thread puis
     * ajoute le jeu de balle en tant qu'obsverveur
     *
     * @param jdb la fenetre jeu de balle
     */
    public void addTortueBalle(JeuDeBalle jdb) {
        int numero = new Random().nextInt(12);
        Tortue t = new TortueBalle(PartieDeFoot.getLongeurTerrain() / 2, PartieDeFoot.getLargeurTerrain() / 2 + 20, 0, 0, Constante.decodeColor(numero));
        TortueRunnable tr = new TortueRunnable(t, this);
        mapTortuesAndThreads.put(t, tr);
        t.addObserver(jdb);
        ajouterTortuesConnues();
        if (this.jeuLance) {
            tr.getThread().start();
            tr.setFin(false);
            if (!this.jeuEnCours) {
                tr.pause();
            }
        }
    }

    /**
     *
     * @return une nouvelle instance de stratégie collective
     */
    public Collectif creerStrategieCollectif() {
        return new Collectif();
    }

    /**
     *
     * @return une nouvelle instance de stratégie personnelle
     */
    public Personnel creerStrategiePersonnel() {
        return new Personnel();
    }

    /**
     *
     * @return une nouvelle instance de stratégie tir joueur
     */
    public TirJoueur creerStrategieTirJoueur() {
        return new TirJoueur();
    }

    /**
     *
     * @return une nouvelle instance de stratégie attaquante
     */
    public Attaquant creerStrategieAttaquant() {
        return new Attaquant();
    }

    /**
     *
     * @return une nouvelle instance de stratégie défensive
     */
    public Defenseur creerStrategieDefenseur() {
        return new Defenseur();
    }

    /**
     *
     * @return une nouvelle instance de stratégie de gardien
     */
    public Gardien creerStrategieGardien() {
        return new Gardien();
    }

    /**
     *
     * @return une nouvelle instance de stratégie de milieu de terrain
     */
    public Milieu creerStrategieMilieu() {
        return new Milieu();
    }

    /**
     *
     * @return une nouvelle instance de stratégie de joueur 1 controllable
     */
    public Joueur1 creerStrategieJoueur1() {
        return new Joueur1();
    }

    /**
     *
     * @return une nouvelle instance de stratégie de joueur 2 controllable
     */
    public Joueur2 creerStrategieJoueur2() {
        return new Joueur2();
    }

    /**
     * Met à jour les informations sur une tortue. Si une stratégie de Joueur
     * controllable est mise à jour, on ajoute ou supprime la tortue de la liste
     * des tortues controllables
     *
     * @param t la tortue améliorée à mettre à jour
     * @param nom le nouveau nom
     * @param strategieCooperation la nouvelle stratégie de coopération
     * @param strategiePosition la nouvelle stratégie de positionnement
     * @param vitesse la nouvelle vitesse
     */
    public void changerCaracteristique(TortueAmelioree t, String nom, Cooperation strategieCooperation, Position strategiePosition, int vitesse) {
        // Mise à jour des tourtues controllables
        if ((t.getStrategie().getPosition() instanceof Joueur1 || strategiePosition instanceof Joueur1
                || t.getStrategie().getPosition() instanceof Joueur2 || strategiePosition instanceof Joueur2) && (!(t.getStrategie().getPosition() instanceof Joueur1 && (strategiePosition instanceof Joueur1 || strategiePosition instanceof Joueur2))
                && !(t.getStrategie().getPosition() instanceof Joueur2 && (strategiePosition instanceof Joueur1 || strategiePosition instanceof Joueur2)))) {

            TortueRunnable tr = this.mapTortuesAndThreads.get(t);
            // Suppression d'une tortue controllable
            if (t.getStrategie().getPosition() instanceof Joueur1 || t.getStrategie().getPosition() instanceof Joueur2) {
                this.listeTortuesControllables.remove(tr);
            } else {
                // Ajout d'une tortue controllable
                this.listeTortuesControllables.add(tr);
            }
        }
        t.setNom(nom);
        t.getStrategie().setCooperation(strategieCooperation);
        t.getStrategie().setPosition(strategiePosition);
        t.setVitesse(vitesse);

    }

    /**
     * Lance tous les Threads des tortues de la liste mapTortuesAndThreads
     */
    public void runAll() {
        if (!this.jeuLance) {
            for (Entry<Tortue, TortueRunnable> entry : this.mapTortuesAndThreads.entrySet()) {
                if (entry.getValue().getThread().getState() == Thread.State.NEW) {
                    entry.getValue().getThread().start();
                }
                if (entry.getValue().isFin()) {
                    entry.getValue().setFin(false);
                }
            }
            this.jeuLance = true;
            this.jeuEnCours = true;
        }
    }

    /**
     * Met en pause tous les Threads des tortues de la liste
     * mapTortuesAndThreads
     */
    public void pauseAll() {
        if (this.jeuLance) {
            for (Entry<Tortue, TortueRunnable> entry : this.mapTortuesAndThreads.entrySet()) {
                entry.getValue().pause();
            }
            this.jeuEnCours = false;
        }
    }

    /**
     * Reprise de tous les Threads des tortues de la liste mapTortuesAndThreads
     */
    public void restartAll() {
        if (this.jeuLance) {
            for (Entry<Tortue, TortueRunnable> entry : this.mapTortuesAndThreads.entrySet()) {
                entry.getValue().play();
            }
            this.jeuEnCours = true;
        }
    }

    /**
     * Met en pause le Thread d'une tortue
     *
     * @param t la tortue à mettre en pause
     */
    public void pauseOne(Tortue t) {
        if (this.jeuLance) {
            this.mapTortuesAndThreads.get(t).pause();
        }
    }

    /**
     * Reprise du Thread d'une tortue
     *
     * @param t la tortue à redémarrer
     */
    public void restartOne(Tortue t) {
        if (this.jeuLance) {
            this.mapTortuesAndThreads.get(t).play();
        }
    }

    /**
     * Fermeture de tous les Threads des tortues de la liste
     * mapTortuesAndThreads
     */
    public void killAll() {
        if (this.jeuLance) {
            for (Entry<Tortue, TortueRunnable> entry : this.mapTortuesAndThreads.entrySet()) {
                entry.getValue().setFin(true);
                Thread t = entry.getValue().getThread();
                if (t.isAlive()) {
                    try {
                        t.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
            this.jeuEnCours = false;
        }
    }
}
