/*
 * Utilisation des FPS
 * http://www.java-gaming.org/index.php?topic=24220.0
 */
package Controleur;

import Modele.Tortue;
import Modele.TortueAmelioree;
import Modele.TortueBalle;
import Vue.FenetrePrincipale;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Le Thread d'une tortue
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class TortueRunnable implements Runnable {

    private final int RAYON_DEMARQUAGE = 50;
    private Tortue tortue;
    private boolean execution = true, fin = true;
    private Thread thread;
    private Controleur controleur;
    private ArrayList<Integer> touchesClavier = new ArrayList();

    /**
     * Crée un Thread pour la tortue
     *
     * @param tortue la tortue pour laquelle le Thread est créé
     * @param controleur le contrôleur de l'application
     */
    public TortueRunnable(Tortue tortue, Controleur controleur) {
        this.tortue = tortue;
        this.controleur = controleur;
        this.thread = new Thread(this);
    }

    /**
     *
     * @return le Thread de la tortue
     */
    public Thread getThread() {
        return thread;
    }

    /**
     *
     * @param thread le Thread de la tortue
     */
    public void setThread(Thread thread) {
        this.thread = thread;
    }

    /**
     *
     * @return le contrôleur de l'application
     */
    public Controleur getControleur() {
        return controleur;
    }

    /**
     *
     * @param controleur le contrôleur de l'application
     */
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    /**
     *
     * @return la tortue
     */
    public Tortue getTortue() {
        return tortue;
    }

    /**
     *
     * @param tortue la tortue
     */
    public void setTortue(Tortue tortue) {
        this.tortue = tortue;
    }

    /**
     *
     * @return vrai si le Thread doit être exécuté
     */
    public boolean isExecution() {
        return execution;
    }

    /**
     *
     * @param execution vrai si le Thread doit être exécuté
     */
    public void setExecution(boolean execution) {
        this.execution = execution;
    }

    /**
     *
     * @return vrai si le Thread doit se terminer
     */
    public boolean isFin() {
        return fin;
    }

    /**
     *
     * @param fin vrai si le Thread doit se terminer
     */
    public void setFin(boolean fin) {
        this.fin = fin;
    }

    /**
     * Lance le Thread et notifie ses observeurs
     */
    public void play() {
        execution = true;
        synchronized (this) {
            notify();
        };
    }

    /**
     * Met le Thread en pause
     */
    public void pause() {
        execution = false;
    }

    /**
     * Lance le Thread. Effectue la boucle puis attend X secondes correspondant
     * aux FPS (Frame per second) de la tortue
     * <ul>
     * <li>Déplace la tortue selon sa stratégie</li>
     * </ul>
     * Pour une tortue améliorée:
     * <ul>
     * <li>Met à jour sa situation de démarquage</li>
     * <li>Frappe la balle si elle est proche</li>
     * </ul>
     * Pour une tortue balle:
     * <ul>
     * <li>Vérifie si elle est dans les cages</li>
     * </ul>
     */
    @Override
    public void run() {
        try {

            // Initialisation du timer
            long tempsDerniereBoucle = System.nanoTime();

            while (!fin) {
                if (!execution) {
                    synchronized (this) {
                        try {
                            wait();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(FenetrePrincipale.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else {

                    // Vitesse de la tortue
                    int FPS_CIBLE = tortue.getVitesse();
                    if (FPS_CIBLE != 0) {
                        long TEMPS_OPTIMAL = 1000000000 / FPS_CIBLE;

                        // Récupération du temps courant
                        long now = System.nanoTime();
                        long tempsMaj = now - tempsDerniereBoucle;
                        tempsDerniereBoucle = now;
                        double delta = tempsMaj / ((double) TEMPS_OPTIMAL);

                        // Pause
                        long tempsAttente = (tempsDerniereBoucle - System.nanoTime() + TEMPS_OPTIMAL) / 1000000;
                        if (tempsAttente <= 0) {
                            tempsAttente = 1000;
                        }
                        Thread.sleep(tempsAttente);

                        // Déplace la tortue
                        // Ajoute les touches du clavier pour les joueurs controllables
                        tortue.deplacer(this.touchesClavier);

                        // Frappe la balle
                        if (this.tortue instanceof TortueAmelioree) {
                            ((TortueAmelioree) this.tortue).majDemarque(RAYON_DEMARQUAGE);
                            ((TortueAmelioree) this.tortue).frapperBalle();
                        } else if (this.tortue instanceof TortueBalle && this.controleur.isJeuDeFoot()) {
                            // Verifie si la balle est dans les cages
                            if (((TortueBalle) this.tortue).isInRectangle(this.controleur.getBut1())) {
                                this.controleur.updateScoreEquipe2();
                                this.controleur.reInitialiserTortues();
                            } else if (((TortueBalle) this.tortue).isInRectangle(this.controleur.getBut2())) {
                                this.controleur.updateScoreEquipe1();
                                this.controleur.reInitialiserTortues();
                            }
                        }
                    }
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(TortueAmelioree.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Ajoute le code de la touche pressée dans touchesClavier
     *
     * @param e touche du clavier pressée
     */
    public void ajouterToucheClavier(KeyEvent e) {
        if (!this.touchesClavier.contains(e.getKeyCode())) {
            this.touchesClavier.add(e.getKeyCode());
        }
    }

    /**
     * Supprime le code de la touche pressée dans touchesClavier
     *
     * @param e touche du clavier relâchée
     */
    public void supprimerToucheClavier(KeyEvent e) {
        if (this.touchesClavier.contains(e.getKeyCode())) {
            this.touchesClavier.remove((Integer) e.getKeyCode());
        }
    }
}
