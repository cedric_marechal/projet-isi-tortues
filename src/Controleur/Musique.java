package Controleur;

import java.io.*;
import javax.sound.sampled.*;

/**
 * Utilitaire jouant des sons et musiques dans un Thread
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class Musique implements Runnable {

    private Thread player;
    private String fileLocation;
    private boolean loop;

    /**
     * Crée un Thread pour une musique
     *
     * @param fileLocation le chemin d'accès au fichier
     * @param loop vrai si la musique doit être jouée en boucle
     */
    public Musique(String fileLocation, boolean loop) {
        this.fileLocation = fileLocation;
        this.loop = loop;
        this.player = new Thread(this);
    }

    /**
     *
     * @return le Thread jouant la musique
     */
    public Thread getPlayer() {
        return player;
    }

    /**
     *
     * @param player le Thread jouant la musique
     */
    public void setPlayer(Thread player) {
        this.player = player;
    }

    /**
     *
     * @return le chemin d'accès au fichier
     */
    public String getFileLocation() {
        return fileLocation;
    }

    /**
     *
     * @param fileLocation le chemin d'accès au fichier
     */
    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    /**
     *
     * @return vrai si la musique doit être jouée en boucle
     */
    public boolean isLoop() {
        return loop;
    }

    /**
     *
     * @param loop vrai si la musique doit être jouée en boucle
     */
    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    /**
     * Lance le Thread et joue le fichier spécifié par le chemin d'accès
     */
    @Override
    public void run() {
        playSound(fileLocation);
    }

    /**
     * Lit le fichier puis émet un sortie audio, réitère tant que loop est à
     * vrai
     *
     * @param fileName le chemin d'accès au fichier
     */
    private void playSound(String fileName) {
        while (this.loop) {
            File soundFile = new File(fileName);
            AudioInputStream audioInputStream = null;
            try {
                audioInputStream = AudioSystem.getAudioInputStream(soundFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            AudioFormat audioFormat = audioInputStream.getFormat();
            SourceDataLine line = null;
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
            try {
                line = (SourceDataLine) AudioSystem.getLine(info);
                line.open(audioFormat);
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            line.start();
            int nBytesRead = 0;
            byte[] abData = new byte[128000];

            while (nBytesRead != -1) {
                try {
                    nBytesRead = audioInputStream.read(abData, 0, abData.length);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (nBytesRead >= 0) {
                    int nBytesWritten = line.write(abData, 0, nBytesRead);
                }
            }

            line.drain();
            line.close();
        }
    }
}