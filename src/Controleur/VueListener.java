package Controleur;

import Modele.TortueAmelioree;
import Vue.CaracteristiqueTortue;
import Vue.FenetrePrincipale;
import Vue.FeuilleDessinFoot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Un listeneur sur la fenêtre principale
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class VueListener implements ActionListener {

    private Controleur controleur;
    private FenetrePrincipale fenetre;

    /**
     *
     * @param controleur le contrôleur de l'application
     * @param fenetre la fenêtre principale à écouter
     */
    public VueListener(Controleur controleur, FenetrePrincipale fenetre) {
        this.controleur = controleur;
        this.fenetre = fenetre;
    }

    /**
     *
     * @return le contrôleur de l'application
     */
    public Controleur getControleur() {
        return controleur;
    }

    /**
     *
     * @param Controleur le contrôleur de l'application
     */
    public void setControleur(Controleur Controleur) {
        this.controleur = Controleur;
    }

    /**
     *
     * @return la fenêtre principale à écouter
     */
    public FenetrePrincipale getFenetre() {
        return fenetre;
    }

    /**
     *
     * @param fenetre la fenêtre principale à écouter
     */
    public void setFenetre(FenetrePrincipale fenetre) {
        this.fenetre = fenetre;
    }

    /**
     * Gère les actions suivantes
     * <ul>
     * <li>NewTortue : Crée une nouvelle tortue</li>
     * <li>Start : Lance les Threads de toutes les tortues et la musique si
     * c'est un jeu de foot</li>
     * <li>Pause : Met en pause les Threads de toutes les tortues et la musique
     * si c'est un jeu de foot</li>
     * <li>Reprise : Relance les Threads de toutes les tortues et la musique si
     * c'est un jeu de foot</li>
     * <li>StartJeuDeBalle: Ferme tous les Threads puis initialise un jeu de
     * balle </li>
     * <li>StartJeuDeBalle: Ferme tous les Threads puis initialise un jeu de
     * balle </li>
     * <li>Valider: Ouvre la fenêtre des caractéristiques de la tortue</li>
     * <li>Quitter: Ferme l'application</li>
     * </ul>
     *
     * @param e l'venement généré sur la fenêtre
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String c = e.getActionCommand();
        System.out.println("commande: " + c);
        if (c.equals("+Joueur")) {
            this.controleur.addTortueAmelioree(fenetre.getFeuille());
        }if (c.equals("+Balle")) {
            this.controleur.addTortueBalle(fenetre.getFeuille());
        } else if (c.equals("Reset")) {
            this.fenetre.dispose();
            new FenetrePrincipale(this.controleur);
        } else if (c.equals("Start")) {
            this.controleur.runAll();
            if (controleur.isJeuDeFoot() && this.controleur.getMusique().getPlayer().getState() == Thread.State.NEW) {
                this.controleur.getMusique().getPlayer().start();
            }
        } else if (c.equals("Pause")) {
            this.controleur.pauseAll();
            if (this.controleur.isJeuDeFoot()) {
                this.controleur.getMusique().getPlayer().suspend();
            }
        } else if (c.equals("Reprise")) {
            this.controleur.restartAll();
            if (this.controleur.isJeuDeFoot()) {
                this.controleur.getMusique().getPlayer().resume();
            }
        } else if (c.equals("StartJeuDeBalle")) {
            this.controleur.getMusique().getPlayer().suspend();
            this.controleur.setJeuDeFoot(false);
            this.controleur.killAll();
            this.controleur.initialiserTortuesJeuDeBalle();
            this.fenetre.loadJeuDeBalle();
        } else if (c.equals("StartJeuDeFoot")) {
            this.controleur.getMusique().getPlayer().resume();
            this.controleur.setJeuDeFoot(true);
            this.controleur.killAll();
            this.controleur.initialiserTortuesJeuDeFoot();
            this.fenetre.loadJeuDeFoot();
        } else if (c.equals("StartJeuDeTennis")) {
            this.controleur.setJeuDeFoot(false);
            this.controleur.killAll();
            this.controleur.initialiserTortuesJeuDeTennis();
            this.fenetre.loadJeuDeTennis();
        } else if (c.equals("Valider")) {
            if (this.fenetre.getFeuille() instanceof FeuilleDessinFoot) {
                FeuilleDessinFoot feuille = (FeuilleDessinFoot) this.fenetre.getFeuille();
                TortueAmelioree tortue = (TortueAmelioree) feuille.getListeJoueurs().getSelectedItem();
                CaracteristiqueTortue newFenetre = new CaracteristiqueTortue(this.controleur, tortue);
                newFenetre.setVisible(true);
            }
        } else if (c.equals("Stop Musique")) {
            this.controleur.getMusique().getPlayer().stop();
        } else if (c.equals("Quitter")) {
            System.exit(0);
        }

    }
}
