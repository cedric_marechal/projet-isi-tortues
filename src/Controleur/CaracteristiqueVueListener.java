package Controleur;

import Modele.Strategie.Cooperation.Cooperation;
import Modele.Strategie.Position.Position;
import Vue.CaracteristiqueTortue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Un listener sur les éléments de la fenêtre des caractéristiques de la tortue
 *
 * @author Cédric, Matthias, Evgeniy, Amaury
 */
public class CaracteristiqueVueListener implements ActionListener {

    private Controleur controleur;
    private CaracteristiqueTortue fenetre;

    /**
     * Crée un listener sur les éléments de la fenêtre des caractéristiques de
     * la tortue
     *
     * @param controleur le controleur de l'application
     * @param fenetre la fenêtre de caractéristiques à écouter
     */
    public CaracteristiqueVueListener(Controleur controleur, CaracteristiqueTortue fenetre) {
        this.controleur = controleur;
        this.fenetre = fenetre;
    }

    /**
     *
     * @return le controleur de l'application
     */
    public Controleur getControleur() {
        return controleur;
    }

    /**
     *
     * @param controleur le contrôleur de l'application
     */
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    /**
     *
     * @return la fenêtre écoutée
     */
    public CaracteristiqueTortue getFenetre() {
        return fenetre;
    }

    /**
     *
     * @param fenetre la fenêtre écoutée
     */
    public void setFenetre(CaracteristiqueTortue fenetre) {
        this.fenetre = fenetre;
    }

    /**
     * Applique les changements de caractéristiques (sans fermer la fanêtre)
     * Valide les changements de caractéristiques (en fermant la fanêtre)
     *
     * @param e l'venement généré sur la fenêtre
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String c = e.getActionCommand();
        System.out.println(c);
        if (c.equals("Appliquer")) {
            System.out.println("Changement des caracteristique de la tortue : " + this.fenetre.getTortue().getNom());
            this.controleur.changerCaracteristique(this.fenetre.getTortue(), this.fenetre.getNom().getText(), (Cooperation) this.fenetre.getListeStrategieCooperation().getSelectedItem(), (Position) this.fenetre.getListeStrategiePosition().getSelectedItem(), this.fenetre.getSliderVitesse().getValue());
        } else if (c.equals("Valider")) {
            System.out.println("Changement des caracteristique de la tortue : " + this.fenetre.getTortue().getNom());
            this.controleur.changerCaracteristique(this.fenetre.getTortue(), this.fenetre.getNom().getText(), (Cooperation) this.fenetre.getListeStrategieCooperation().getSelectedItem(), (Position) this.fenetre.getListeStrategiePosition().getSelectedItem(), this.fenetre.getSliderVitesse().getValue());
            this.fenetre.dispose();
        }
    }
}
